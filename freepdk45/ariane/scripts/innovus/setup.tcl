set vars(max_route_layer) 7
set vars(cpf_file)        "$::env(FF_DESIGN_SCRIPTS_DIR)/power_intent.cpf"

foreach lib $vars(library_sets) {
    append vars($lib,timing) " [glob ./memory/*.lib]"
}

append vars(lef_files) " [glob ./memory/*.lef]"
