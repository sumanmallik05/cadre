set margin 3 ;# Set specifically to avoid dangling stripes in the margin
set spacing 4
set bigSpace 112

# Lower Right Corner
# ##############################################################################
placeInstance be_mmu/dcache/data_mem_7__data_mem/macro_mem/mem \
  [expr [dbGet top.fPlan.coreBox_urx] - [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_7__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90
placeInstance be_mmu/dcache/data_mem_6__data_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_7__data_mem/macro_mem/mem].box_llx] - [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_6__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90
placeInstance be_mmu/dcache/data_mem_5__data_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_6__data_mem/macro_mem/mem].box_llx] - [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_5__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90
placeInstance be_mmu/dcache/data_mem_4__data_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_5__data_mem/macro_mem/mem].box_llx] - [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_4__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90

placeInstance be_mmu/dcache/data_mem_3__data_mem/macro_mem/mem \
  [expr [dbGet top.fPlan.coreBox_urx] - [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_3__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_7__data_mem/macro_mem/mem].box_ury]] \
  MX90
placeInstance be_mmu/dcache/data_mem_2__data_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_3__data_mem/macro_mem/mem].box_llx] - [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_2__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_6__data_mem/macro_mem/mem].box_ury]] \
  MX90
placeInstance be_mmu/dcache/data_mem_1__data_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_2__data_mem/macro_mem/mem].box_llx] - [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_1__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_5__data_mem/macro_mem/mem].box_ury]] \
  MX90
placeInstance be_mmu/dcache/data_mem_0__data_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_1__data_mem/macro_mem/mem].box_llx] - [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_0__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/data_mem_4__data_mem/macro_mem/mem].box_ury]] \
  MX90

# Lower Middle
# ##############################################################################

placeInstance be_mmu/dcache/tag_mem/macro_mem/mem \
  [expr [dbGet top.fPlan.coreBox_llx] + $bigSpace] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90
placeInstance be_mmu/dcache/stat_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name be_mmu/dcache/tag_mem/macro_mem/mem].box_urx] + $spacing] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90


selectInst [join "[dbGet -e [dbGet -p2 top.insts.cell.name $::env(PLATFORM)_*].name]"]
cutRow -selected -leftGap  $spacing -rightGap $spacing -topGap $spacing -bottomGap $spacing
deselectAll