####################################################################################
#                           PRE-INIT PLUG-IN
####################################################################################
#
# This plug-in script is called before initializing the design database from the
# run_init.tcl flow script.
#
####################################################################################
#
# Ex: Below command used for inserting buffers on tie-high/tie-low assign statements
#setImportMode -bufferTieAssign $vars(buffer_tie_assign)#
#-----------------------------------------------------------------------------------


# set_units -time ps -resistance kOhm -capacitance fF -voltage V -current uA
setLibraryUnit -cap 1fF -time 1ps
