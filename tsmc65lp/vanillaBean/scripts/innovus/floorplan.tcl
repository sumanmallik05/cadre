####################################################################################
#                             FLOORPLAN SCRIPT
####################################################################################

floorPlan -su $core_aspect_ratio \
             $core_density \
             $core_margin_l \
             $core_margin_b \
             $core_margin_r \
             $core_margin_t

setFlipping s

################################################################################
# MACRO PLACEMENT
################################################################################


# Define cell sizes
set mem_x [dbGet [dbGetCellByName tsmc65lp_1rf_lg10_w32_all].size_x]

# Macro instance list
set mem "[dbGet [dbGet -p2 top.insts.cell.name tsmc65lp_1rf_lg10_w32_all].name]
         [dbGet [dbGet -p2 top.insts.cell.name tsmc65lp_1rf_lg10_w32_byte].name]
         [dbGet [dbGet -p2 top.insts.cell.name tsmc65lp_2rf_lg5_w32_all].name]"

set die_width [dbGet top.fplan.box_sizex]

# Place instances
set margin 0
set spacing 5

placeInstance [lindex $mem 0] \
  [expr [dbGet top.fplan.coreBox_llx] + $margin] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R90
placeInstance [lindex $mem 1] \
  [expr [dbGet top.fplan.coreBox_urx] - [dbGet [dbGet -p1 top.insts.name [lindex $mem 1]].box_sizey] - $margin] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R270

placeInstance [lindex $mem 2] \
  [expr [dbGet top.fplan.coreBox_llx] + [dbGet top.fplan.coreBox_sizex]/2 - [dbGet [dbGet -p1 top.insts.name [lindex $mem 2]].box_sizey]] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R90

placeInstance [lindex $mem 3] \
  [expr [dbGet [dbGet -p1 top.insts.name [lindex $mem 2]].box_urx] + $spacing] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R90

selectInst [join "[dbGet -e [dbGet -p2 top.insts.cell.name $::env(PLATFORM)_*].name]"]
cutRow -selected -leftGap  $spacing -rightGap $spacing -topGap $spacing -bottomGap $spacing
deselectAll

################################################################################
# I/O PIN PLACEMENT
################################################################################

# Create pin lists
selectPin link_out*
set link_out_pins [dbGet selected.name]
deselectAll
selectPin link_in*
set link_in_pins  [dbGet selected.name]
deselectAll

# Get number of link_in/link_out pins per side
set num_links [expr [llength $link_out_pins] / 4]

# Set side and pin indicies based on pin ordering
set sides  {      LEFT         RIGHT      TOP         BOTTOM     }
set dir    {counterclockwise clockwise clockwise counterclockwise}
set layers {       M5           M5        M4            M4       }

# Spread pins along each side according to processor link direction
for {set side_idx 0} {$side_idx < 4} {incr side_idx} {
    set start_idx [expr ($side_idx  ) * $num_links    ]
    set end_idx   [expr ($side_idx+1) * $num_links - 1]

    set in_subset  [lrange $link_in_pins  $start_idx $end_idx]
    set out_subset [lrange $link_out_pins $start_idx $end_idx]

    # clear for this iteration
    unset -nocomplain side_subset

    foreach in_element $in_subset out_element $out_subset {
        # Need to make sure the in/out order is flipped on opposite sides so that pins line up when abutted
        if {$side_idx == 1 || $side_idx == 3} {
            lappend side_subset $in_element $out_element
        } else {
            lappend side_subset $out_element $in_element
        }
    }

    if {$side_idx <= 1} {
        editPin -pin $side_subset -fixedPin 1 -layer [lindex $layers $side_idx] \
                -snap TRACK -side [lindex $sides $side_idx] -spreadDirection [lindex $dir $side_idx] -spreadType SIDE -use SIGNAL
    } else {
        editPin -pin $side_subset -fixedPin 1 -layer [lindex $layers $side_idx] \
                -snap TRACK -side [lindex $sides $side_idx] -spreadDirection [lindex $dir $side_idx] -spreadType RANGE -start "$mem_x 0" -end "[expr $die_width - $mem_x]" 0 -use SIGNAL
    }
}

# Assign locations for other pins
selectPin "my_* reset_i"
set side_subset [dbGet selected.name]

editPin -pin $side_subset -fixedPin 1 -layer M6 -snap TRACK -side BOTTOM -spreadType CENTER -use SIGNAL -spacing 16 -unit TRACK
editPin -pin "clk_i" -fixedPin 1 -layer M6 -snap TRACK -side BOTTOM -spreadType START -use CLOCK -start "[expr $die_width / 2 + 8] 0"
