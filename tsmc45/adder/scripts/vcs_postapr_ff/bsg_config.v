config bsg_config;

  // top level testbench
  design `BSG_TESTING_LIBRARY_NAME.`BSG_TOP_SIM_MODULE;

  // library priority
  default liblist `BSG_TESTING_LIBRARY_NAME work;

  // bsg_chip code in chip library
  instance `BSG_CHIP_INSTANCE_PATH liblist `BSG_CHIP_LIBRARY_NAME;

  instance test_five.dut.chip.g.acc.bnn_rocc_inst liblist bnn_library

  cell bsg_manycore_tile liblist tile_library
  cell bsg_manycore_tile_1024_1_1024_1_3_32_20_0_00000000_00000000_0 liblist dcdctile_library

  cell pll liblist pll_library

  instance test_five.dut.chip.g.acc.manycore_rocc_inst.UUT liblist manycore_library
  instance test_five.dut.chip.g.dcdc_block.dcdc_manycore liblist dcdc_manycore_library

  instance test_five.dut.chip.g.n_0__clnt liblist coyote_library
  instance test_five.dut.chip.g.n_1__clnt liblist coyote_library
  instance test_five.dut.chip.g.n_2__clnt liblist coyote_library
  instance test_five.dut.chip.g.n_3__clnt liblist coyote_library
  instance test_five.dut.chip.g.n_4__clnt liblist coyote_library

endconfig
