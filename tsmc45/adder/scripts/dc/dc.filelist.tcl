# If using bsg_designs, grab the filelist from the repo
# TODO fix this to work without BSG
if {0 &&[info exists ::env(BSG_DESIGNS_TARGET)] && $::env(BSG_DESIGNS_TARGET) ne ""} {
    proc bsg_get_module_name {module_path} {
      regexp {[A-Za-z0-9_]+\.v} $module_path module_name
      return $module_name
    }
    
    # Grab the target bsg design filelist (defines SVERILOG_SOURCE_FILES)
    source $::env(BSG_DESIGNS_TARGET_DIR)/tcl/filelist.tcl
    
    # Grab the asic_hard_filelist, NEW_SVERILOG_SOURCE_FILES and NETLIST_SOURCE_FILES
    source $::env(BSG_DESIGNS_TARGET_DIR)/tcl/hard/$::env(BSG_DESIGNS_HARD_TARGET)/filelist_deltas.tcl
    
    # Get a list of all the filenames to swap in
    set asic_hard_module_list [list]
    foreach f $asic_hard_filelist {
      lappend asic_hard_module_list [bsg_get_module_name $f]
    }
    
    # Find SVERILOG_SOURCE_FILES that have the same filename and
    # replace them with those found in asic_hard_filelist
    foreach f $SVERILOG_SOURCE_FILES {
      set asic_module_name [bsg_get_module_name $f]
      set idx [lsearch $asic_hard_module_list $asic_module_name]
      if {$idx == -1} {
        lappend tmp_list $f
      } else {
        lappend tmp_list [lindex $asic_hard_filelist $idx]
      }
    }
    
    # List of sverilog files. This includes the swapped in harden files as well as
    # any new sverilog files defined in filelist_deltas.tcl
    set SVERILOG_SOURCE_FILES [concat $tmp_list $NEW_SVERILOG_SOURCE_FILES]

# Else, use a custom filelist
} else {
    set SVERILOG_SOURCE_FILES [join "
        $::env(DESIGN_ROOT_DIR)/src/adder.v
    "]
}
