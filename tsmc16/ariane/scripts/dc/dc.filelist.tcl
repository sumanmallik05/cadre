# File order matters
set SVERILOG_SOURCE_FILES "
$::env(DESIGN_ROOT_DIR)/src/include/riscv_pkg.sv
$::env(DESIGN_ROOT_DIR)/src/include/dm_pkg.sv
$::env(DESIGN_ROOT_DIR)/src/include/ariane_pkg.sv
$::env(DESIGN_ROOT_DIR)/src/include/axi_pkg.sv
$::env(DESIGN_ROOT_DIR)/src/include/ariane_axi_pkg.sv
$::env(DESIGN_ROOT_DIR)/src/include/std_cache_pkg.sv

$::env(DESIGN_ROOT_DIR)/src/verilog/alu.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/amo_buffer.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/ariane.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/ariane_regfile_ff.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/axi_adapter.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/branch_unit.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/cache_subsystem/std_cache_subsystem.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/cache_subsystem/std_icache.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/cache_subsystem/std_nbdcache.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/cache_subsystem/cache_ctrl.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/cache_subsystem/miss_handler.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/cache_subsystem/tag_cmp.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/cache_subsystem/amo_alu.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/commit_stage.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/common_cells/src/fifo_v2.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/common_cells/src/fifo_v3.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/common_cells/src/lfsr_8bit.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/common_cells/src/lzc.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/common_cells/src/pipe_reg_simple.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/common_cells/src/rrarbiter.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/common_cells/src/stream_arbiter.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/common_cells/src/stream_demux.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/common_cells/src/stream_mux.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/compressed_decoder.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/controller.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/csr_buffer.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/csr_regfile.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/decoder.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/ex_stage.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/frontend/bht.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/frontend/btb.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/frontend/frontend.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/frontend/instr_scan.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/frontend/ras.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/id_stage.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/instr_realigner.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/issue_read_operands.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/issue_stage.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/load_store_unit.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/load_unit.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/mmu.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/mult.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/multiplier.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/perf_counters.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/ptw.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/re_name.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/scoreboard.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/serdiv.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/store_buffer.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/store_unit.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/tlb.sv

$::env(DESIGN_ROOT_DIR)/src/verilog/sram.sv
$::env(DESIGN_ROOT_DIR)/src/verilog/SyncSpRamBeNx64.sv
"


set NETLIST_SOURCE_FILES ""

