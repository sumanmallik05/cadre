setOptMode -fixFanoutLoad true

################################################################################
#                          PRE-PLACE PLUG-IN
################################################################################
#
# This plug-in script is called before placeDesign from the run_place.tcl flow
# script.
#
################################################################################
# Example tasks include:
#          - Power planning related tasks which includes
#            - Power planning for power domains (ring/strap creations)
#            - Power Shut-off cell power hookup
################################################################################


# TODO: temporary - create a core ring for connectivity
addRing -nets {VDD VSS} -type core_rings -follow core \
    -layer {top M8 bottom M8 left M9 right M9} \
    -width $p_rng_w -spacing $p_rng_s -offset $p_rng_s \
    -extend_corner {tl tr bl br lt lb rt rb}


source $vars(cadre,power_plan_tcl)

# editSelect -net {VSS VDD}
# editTrim -selected
# deselectAll
