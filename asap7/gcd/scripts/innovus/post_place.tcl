# Decap Cells from ASAP7 don't list the capacitance
# addDeCapCellCandidates DECAPx10_ASAP7_75t_R ?
# addDeCapCellCandidates DECAPx6_ASAP7_75t_R ?
# addDeCapCellCandidates DECAPx4_ASAP7_75t_R ?

#addDeCap -cells {DECAPx10_ASAP7_75t_R DECAPx6_ASAP7_75t_R DECAPx4_ASAP7_75t_R} -totCap 500 -effort high
