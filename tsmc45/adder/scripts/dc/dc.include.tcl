# If using bsg_designs, grab the include.tcl from the repo
if {[info exists ::env(BSG_DESIGNS_TARGET)] && $::env(BSG_DESIGNS_TARGET) ne ""} {
    # Grab the search paths for the bsg_design (defines SVERILOG_INCLUDE_PATHS)
    source $::env(BSG_DESIGNS_TARGET_DIR)/tcl/include.tcl

    # set generic and hard iopads folders for replacement
    set generic_iopads_dir $::env(BSG_PACKAGING_DIR)/common/foundry/portable/verilog
    set hard_iopads_dir    $::env(BSG_PACKAGING_DIR)/common/foundry/$::env(BSG_PACKAGING_FOUNDRY)/verilog

    if {$::env(MODULE_TYPE)=="chip"} {
        # replace generic iopad folder with hard iopad one
        set idx [lsearch $SVERILOG_INCLUDE_PATHS $generic_iopads_dir]
        set SVERILOG_INCLUDE_PATHS [lreplace $SVERILOG_INCLUDE_PATHS $idx $idx $hard_iopads_dir]
    }

# Else, use a custom filelist
} else {
    set SVERILOG_INCLUDE_PATHS [join "
    "]
}
