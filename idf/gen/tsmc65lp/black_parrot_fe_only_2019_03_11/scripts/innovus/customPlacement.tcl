set margin 3 ;# Set specifically to avoid dangling stripes in the margin
set spacing 4
set bigSpace 112

# Lower Left Corner
# ##############################################################################

placeInstance icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem \
  [expr $margin + [dbGet top.fplan.coreBox_llx]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90
placeInstance icache_1/data_mem_banks_5__data_mem_bank/macro_mem/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem].box_urx]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90
placeInstance icache_1/data_mem_banks_6__data_mem_bank/macro_mem/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name icache_1/data_mem_banks_5__data_mem_bank/macro_mem/mem].box_urx]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90
placeInstance icache_1/data_mem_banks_7__data_mem_bank/macro_mem/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name icache_1/data_mem_banks_6__data_mem_bank/macro_mem/mem].box_urx]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90
placeInstance icache_1/data_mem_banks_0__data_mem_bank/macro_mem/mem \
  [expr $margin + [dbGet top.fplan.coreBox_llx]] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem].box_ury]] \
  R90
placeInstance icache_1/data_mem_banks_1__data_mem_bank/macro_mem/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem].box_urx]] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem].box_ury]] \
  R90
placeInstance icache_1/data_mem_banks_2__data_mem_bank/macro_mem/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name icache_1/data_mem_banks_5__data_mem_bank/macro_mem/mem].box_urx]] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem].box_ury]] \
  R90
placeInstance icache_1/data_mem_banks_3__data_mem_bank/macro_mem/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name icache_1/data_mem_banks_6__data_mem_bank/macro_mem/mem].box_urx]] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem].box_ury]] \
  R90

# Lower Middle
# ##############################################################################
placeInstance bp_fe_pc_gen_1/genblk1_branch_prediction_1/btb_1/btb_mem/macro_mem/mem \
  [expr [dbGet top.fPlan.coreBox_urx] - $bigSpace - [dbGet [dbGet -p1 top.insts.name bp_fe_pc_gen_1/genblk1_branch_prediction_1/btb_1/btb_mem/macro_mem/mem].box_sizey]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90
placeInstance icache_1/tag_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name bp_fe_pc_gen_1/genblk1_branch_prediction_1/btb_1/btb_mem/macro_mem/mem].box_llx] - $spacing - [dbGet [dbGet -p1 top.insts.name icache_1/tag_mem/macro_mem/mem].box_sizey]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90
placeInstance icache_1/metadata_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name icache_1/tag_mem/macro_mem/mem].box_llx] - $spacing - [dbGet [dbGet -p1 top.insts.name icache_1/metadata_mem/macro_mem/mem].box_sizey]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90


selectInst [join "[dbGet -e [dbGet -p2 top.insts.cell.name $::env(PLATFORM)_*].name]"]
cutRow -selected -leftGap  $spacing -rightGap $spacing -topGap $spacing -bottomGap $spacing
deselectAll