
setenv BSG_DESIGNS_TARGET_DIR "$::env(BSG_DESIGNS_DIR)/toplevels/bsg_two_coyote_accum"

source "$::env(BSG_DESIGNS_TARGET_DIR)/tcl/include.tcl"
source "./scripts/dc/dc.filelist.tcl"

set vars(parameters) ""

set vars(bbox) "tsmc16_1r1w_lg6_w88_bit tsmc16_1r1w_lg8_w128_bit tsmc16_1rf_lg6_w80_bit tsmc16_1rf_lg7_w74_all tsmc16_1rf_lg8_w128_all"
