module dcdc_manycore (
	clk_i, 
	reset_i, 
	hor_link_sif_i, 
	hor_link_sif_o, 
	ver_link_sif_i, 
	ver_link_sif_o);
   input clk_i;
   input reset_i;
   input [739:0] hor_link_sif_i;
   output [739:0] hor_link_sif_o;
   input [295:0] ver_link_sif_i;
   output [295:0] ver_link_sif_o;
endmodule
