####################################################################################
#                             FLOORPLAN SCRIPT
####################################################################################

# SET DIE BOX
# ==============================================================================
set die_width     [expr $bondpad_width*20 + $bondpad_spacing*19 + $bondpad_height*2 + $bondpad_margin*2]
set die_height    $die_width

# SET IO BOX
# ==============================================================================
set bondpad_margin_x [expr $bondpad_height + $bondpad_margin]
set bondpad_margin_y [expr $bondpad_height + $bondpad_margin]

# SET CORE BOX
# ==============================================================================
set core_margin [expr 50 * $t_pitch]

# Obtained from IO Cells
set io_cell_H_height 74.5
set io_cell_V_height 71.52

set coreBox_llx [expr 0 + $bondpad_margin_x + $io_cell_H_height + $core_margin]
set coreBox_lly [expr 0 + $bondpad_margin_y + $io_cell_V_height + $core_margin]
set coreBox_urx [expr $die_width - $bondpad_margin_x - $io_cell_H_height - $core_margin]
set coreBox_ury [expr $die_height - $bondpad_margin_y - $io_cell_V_height - $core_margin]


floorPlan -b 0.0000 0.0000 $die_width $die_height \
             $bondpad_margin_x $bondpad_margin_y [expr $die_width - $bondpad_margin_x] [expr $die_height - $bondpad_margin_y] \
             $coreBox_llx $coreBox_lly $coreBox_urx $coreBox_ury

setFlipping s


# IO FLOORPLANNING
# ==============================================================================

# North
addInst -cell PVDD_08_08_NT_DR_V -inst VDD_N
addInst -cell PVSS_08_08_NT_DR_V -inst VSS_N
addInst -cell PDVDD_18_18_NT_DR_V -inst DVDD_N
addInst -cell PDVSS_18_18_NT_DR_V -inst DVSS_N

# East
addInst -cell PVDD_08_08_NT_DR_H -inst VDD_E
addInst -cell PVSS_08_08_NT_DR_H -inst VSS_E
addInst -cell PDVDD_18_18_NT_DR_H -inst DVDD_E
addInst -cell PDVSS_18_18_NT_DR_H -inst DVSS_E

# South
addInst -cell PVDD_08_08_NT_DR_V -inst VDD_S
addInst -cell PVSS_08_08_NT_DR_V -inst VSS_S
addInst -cell PDVDD_18_18_NT_DR_V -inst DVDD_S
addInst -cell PDVSS_18_18_NT_DR_V -inst DVSS_S

# West
addInst -cell PVDD_08_08_NT_DR_H -inst VDD_W
addInst -cell PVSS_08_08_NT_DR_H -inst VSS_W
addInst -cell PDVDD_18_18_NT_DR_H -inst DVDD_W
addInst -cell PDVSS_18_18_NT_DR_H -inst DVSS_W

# Corners
addInst -cell PCORNER_18_18_NT_DR -inst PCORNER_TOPRIGHT
addInst -cell PCORNER_18_18_NT_DR -inst PCORNER_TOPLEFT
addInst -cell PCORNER_18_18_NT_DR -inst PCORNER_BOTTOMLEFT
addInst -cell PCORNER_18_18_NT_DR -inst PCORNER_BOTTOMRIGHT

loadIoFile -noAdjustDieSize $::env(INNOVUS_SCRIPTS_DIR)/floorplan.io

# The GPIO library includes "test" bonding pads but not using them for now
setAttribute -net PAD_N* -skip_routing true
setAttribute -net PAD_E* -skip_routing true
setAttribute -net PAD_S* -skip_routing true
setAttribute -net PAD_W* -skip_routing true


# SNS/RTO handling
# ------------------------------------------------------------------------------
addNet SNS
addNet RTO
# Iterate through all instances that have the pin SNS and attach
foreach cellInst [dbGet [dbGet -p [dbGet -p2 top.insts.instTerms.name */SNS].isPhysOnly 0].name] {
  attachTerm $cellInst SNS SNS
  attachTerm $cellInst RTO RTO
}
setAttribute -net SNS -skip_routing true
setAttribute -net RTO -skip_routing true

################################################################################
# MACRO PLACEMENT
################################################################################


################################################################################
# I/O PIN PLACEMENT
################################################################################

