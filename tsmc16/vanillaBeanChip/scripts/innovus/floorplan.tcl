####################################################################################
#                             FLOORPLAN SCRIPT
####################################################################################

# SET DIE BOX
# ==============================================================================
set die_width     [expr $bondpad_width*20 + $bondpad_spacing*19 + $bondpad_height*2 + $bondpad_margin*2]
set die_height    $die_width

# SET IO BOX
# ==============================================================================
set bondpad_margin_x [expr $bondpad_height + $bondpad_margin]
set bondpad_margin_y [expr $bondpad_height + $bondpad_margin]

# SET CORE BOX
# ==============================================================================
set core_margin [expr 50 * $t_pitch]

# Obtained from IO Cells
set io_cell_H_height 74.5
set io_cell_V_height 71.52

# floorPlan -s $core_width $core_height $core_margin_l $core_margin_b $core_margin_r $core_margin_t
set coreBox_llx [expr $die_width/2 - $core_width/2]
set coreBox_lly [expr $die_height/2 - $core_height/2]
set coreBox_urx [expr $die_width/2 + $core_width/2]
set coreBox_ury [expr $die_height/2 + $core_height/2]


floorPlan -b 0.0000 0.0000 $die_width $die_height \
             $bondpad_margin_x $bondpad_margin_y [expr $die_width - $bondpad_margin_x] [expr $die_height - $bondpad_margin_y] \
             $coreBox_llx $coreBox_lly $coreBox_urx $coreBox_ury

setFlipping s

################################################################################
# MACRO PLACEMENT
################################################################################
# Mimimum memory spacings from ARM sram_sp_hde README

# Minimum memory to stdcell spacing
set min_mem_stdcell_spacing_h 0.540
set min_mem_stdcell_spacing_v 0.672

# Minimum memory to memory spacing
# NOTE: identical memory macros can be abutted on the non-pin edge
set min_mem_mem_spacing_h     1.440
set min_mem_mem_spacing_v     0.672

# Define cell sizes
set mem_x [dbGet [dbGetCellByName tsmc16_1rw_lg10_w32_all].size_x]
set mem_y [dbGet [dbGetCellByName tsmc16_1rw_lg10_w32_all].size_y]

set rf_x  [dbGet [dbGetCellByName tsmc16_2rf_lg5_w32_all].size_x]
set rf_y  [dbGet [dbGetCellByName tsmc16_2rf_lg5_w32_all].size_y]

# Macro instance list
set mem "[dbGet [dbGet -p2 top.insts.cell.name tsmc16_1rw_lg10_w32_all].name]
         [dbGet [dbGet -p2 top.insts.cell.name tsmc16_1rw_lg10_w32_byte].name]
         [dbGet [dbGet -p2 top.insts.cell.name tsmc16_2rf_lg5_w32_all].name]"

# Place instances
set spacing 0

placeInstance [lindex $mem 0] [expr $coreBox_llx + $spacing] [expr $coreBox_lly + $spacing*$t_pitch] MY
placeInstance [lindex $mem 1] [expr $coreBox_urx - $mem_x - $spacing] [expr $coreBox_lly + $spacing*$t_pitch]
placeInstance [lindex $mem 2] [expr $coreBox_llx + $mem_x + $min_mem_mem_spacing_h + 2*$spacing] \
                              [expr $coreBox_lly + $mem_y - $rf_y + $spacing*$t_pitch] MY

# Style 1: RFs on opposite sides
#placeInstance [lindex $mem 3] [expr $core_margin_l + $core_width - $mem_x - (2 * $p_str_w) - (3 * $p_str_s) - $rf_x] \
#                              [expr $core_height + $core_margin_b - $rf_y]

# Style 2: RFs together in UL
placeInstance [lindex $mem 3] [expr $coreBox_llx + $mem_x + (2 * $min_mem_mem_spacing_h) + $rf_x + 3*$spacing] \
                              [expr $coreBox_lly + $mem_y - $rf_y + $spacing*$t_pitch] MY

# IO FLOORPLANNING
# ==============================================================================

# North
addInst -cell PVDD_08_08_NT_DR_V -inst VDD_N
addInst -cell PVSS_08_08_NT_DR_V -inst VSS_N
addInst -cell PDVDD_18_18_NT_DR_V -inst DVDD_N
addInst -cell PDVSS_18_18_NT_DR_V -inst DVSS_N

# East
addInst -cell PVDD_08_08_NT_DR_H -inst VDD_E
addInst -cell PVSS_08_08_NT_DR_H -inst VSS_E
addInst -cell PDVDD_18_18_NT_DR_H -inst DVDD_E
addInst -cell PDVSS_18_18_NT_DR_H -inst DVSS_E

# South
addInst -cell PVDD_08_08_NT_DR_V -inst VDD_S
addInst -cell PVSS_08_08_NT_DR_V -inst VSS_S
addInst -cell PDVDD_18_18_NT_DR_V -inst DVDD_S
addInst -cell PDVSS_18_18_NT_DR_V -inst DVSS_S

# West
addInst -cell PVDD_08_08_NT_DR_H -inst VDD_W
addInst -cell PVSS_08_08_NT_DR_H -inst VSS_W
addInst -cell PDVDD_18_18_NT_DR_H -inst DVDD_W
addInst -cell PDVSS_18_18_NT_DR_H -inst DVSS_W

# Corners
addInst -cell PCORNER_18_18_NT_DR -inst PCORNER_TOPRIGHT
addInst -cell PCORNER_18_18_NT_DR -inst PCORNER_TOPLEFT
addInst -cell PCORNER_18_18_NT_DR -inst PCORNER_BOTTOMLEFT
addInst -cell PCORNER_18_18_NT_DR -inst PCORNER_BOTTOMRIGHT

create_bump \
 -cell PBP40_18_18_NT_DR_MOD \
 -pitch "[expr $bondpad_width + $bondpad_spacing] $bondpad_height" \
 -edge_spacing "[expr $bondpad_height + $bondpad_margin] $bondpad_height [expr $bondpad_height + $bondpad_margin]  $bondpad_height" \
 -pattern_side {top 1} \
 -name_format Bondpad_N%i \
 -start_index 0 \
 -loc_type cell_lowerleft

create_bump \
 -cell PBP40_18_18_NT_DR_MOD \
 -pitch "[expr $bondpad_width + $bondpad_spacing] $bondpad_height" \
 -edge_spacing "[expr $bondpad_height + $bondpad_margin] 0 [expr $bondpad_height + $bondpad_margin] 0" \
 -pattern_side {bottom 1} \
 -name_format Bondpad_S%i \
 -start_index 0 \
 -loc_type cell_lowerleft

create_bump \
 -cell PBP40_18_18_NT_DR_MOD \
 -pitch "$bondpad_height [expr $bondpad_width + $bondpad_spacing]" \
 -edge_spacing "0 [expr $bondpad_height + $bondpad_margin] 0 [expr $bondpad_height + $bondpad_margin]" \
 -pattern_side {left 1} \
 -orientation R90 \
 -name_format Bondpad_W%i \
 -start_index 0 \
 -loc_type cell_lowerleft

create_bump \
 -cell PBP40_18_18_NT_DR_MOD \
 -pitch "$bondpad_height [expr $bondpad_width + $bondpad_spacing]" \
 -edge_spacing "0 [expr $bondpad_height + $bondpad_margin] $bondpad_height [expr $bondpad_height + $bondpad_margin]" \
 -pattern_side {right 1} \
 -orientation R90 \
 -name_format Bondpad_E%i \
 -start_index 0 \
 -loc_type cell_lowerleft


loadIoFile -noAdjustDieSize $::env(INNOVUS_SCRIPTS_DIR)/floorplan.io

# The GPIO library includes "test" bonding pads but not using them for now
setAttribute -net PAD_N* -skip_routing true
setAttribute -net PAD_E* -skip_routing true
setAttribute -net PAD_S* -skip_routing true
setAttribute -net PAD_W* -skip_routing true


# SNS/RTO handling
# ------------------------------------------------------------------------------
addNet SNS
addNet RTO
# Iterate through all instances that have the pin SNS and attach
foreach cellInst [dbGet [dbGet -p [dbGet -p2 top.insts.instTerms.name */SNS].isPhysOnly 0].name] {
  attachTerm $cellInst SNS SNS
  attachTerm $cellInst RTO RTO
}
setAttribute -net SNS -skip_routing true
setAttribute -net RTO -skip_routing true

