################################################################################
#                          PRE-PLACE PLUG-IN
################################################################################
#
# This plug-in script is called before placeDesign from the run_place.tcl flow
# script.
#
################################################################################
# Example tasks include:
#          - Power planning related tasks which includes
#            - Power planning for power domains (ring/strap creations)
#            - Power Shut-off cell power hookup
################################################################################


if {([string tolower $vars(use_pga)] != "true")} {
   source $::env(INNOVUS_FLOW_DIR)/common/tcl/simplePowerPlanGf14.tcl
}


