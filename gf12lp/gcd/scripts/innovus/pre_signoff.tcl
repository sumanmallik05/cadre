
foreach pgNet "VSS VDD" {

  set stripe [dbGet -i 0 -p1 [dbGet [dbGet -p1 top.nets.name $pgNet].sWires].shape followpin]

  createPGPin $pgNet -net $pgNet \
             -geom [dbGet $stripe.layer.name] \
              [dbGet $stripe.box_llx] \
              [dbGet $stripe.box_lly] \
              [expr [dbGet $stripe.box_llx] + [dbGet head.mfgGrid]] \
              [expr [dbGet $stripe.box_lly] + [dbGet head.mfgGrid]]

}
