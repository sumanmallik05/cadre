puts "Info: Running script [info script]\n"

if { ! [file exists ${DESIGN_NAME}_dclib] } { file mkdir ${DESIGN_NAME}_dclib }

# The first "WORK" is a reserved word for Design Compiler.
# The value for -path option is customizable.
define_design_lib WORK -path $::env(DESIGN_ROOT_DIR)/${DESIGN_NAME}_dclib

# Grab the filelist (defines SVERILOG_SOURCE_FILES)
source $::env(DC_SCRIPTS_DIR)/dc.filelist.tcl

puts "Info: Reading the following System Verilog files for synthesis.\n"

# Print one file per line
redirect -tee $::env(DC_LOGS_DIR)/sverilog_filelist_dc.txt {puts "[join $SVERILOG_SOURCE_FILES \n]\n"}

puts "Info: Number of System Verilog files: [llength $SVERILOG_SOURCE_FILES]\n"

# TODO: read in any pre-generated netlist files here (non RTL files)
if {[llength $NETLIST_SOURCE_FILES] > 0} {
  read_verilog -netlist $NETLIST_SOURCE_FILES
}


set PARAMETERS ""

# Analyze and elaborate the design for scheduling (required)
if { ![analyze -define "ASIC SYNTHESIS_HARDWARE NO_DUMMY" -f sverilog $SVERILOG_SOURCE_FILES] } {
  exit 1
}

elaborate ${DESIGN_NAME} -param $PARAMETERS

current_design ${DESIGN_NAME}

# Writing wrapper needed for block instantiation
write_file -format verilog -output ${RESULTS_DIR}/${DESIGN_NAME}.v

puts "Info: Completed script [info script]\n"
