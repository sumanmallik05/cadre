#! /usr/bin/env bash
#=========================================================================
# prune-sram-sdf.sh [sdf-file]
#=========================================================================
# This script comments out certain lines in the SDF that we think are okay
# to ignore. It generates a .bak to compare with.
#
# ## Details
#
# For the ARM SRAMs, the SDF includes some checks that are not in the
# verilog specify blocks. The SDF has entries that look like this:
#
#   (PERIOD (COND FOOBAR (posedge CLK)) (123.123::123.123))
#   (PERIOD (COND FOOBAR (negedge CLK)) (123.123::123.123))
#
# But the Verilog only has entries like this:
#
#     $period(posedge CLK &&& FOOBAR, `ARM_MEM_PERIOD, NOT_CLK_PER);
#
# VCS is complaining because there is nothing like this:
#
#     $period(negedge CLK &&& FOOBAR, `ARM_MEM_PERIOD, NOT_CLK_PER);
#
# Note that we did not find any $period(negedge, in the verilog.
#
# The SDF is checking for a minimum length between two negative clock
# edges (if the COND is true).
#
# We think this is okay to ignore.
#
# Date   : May  4, 2017
# Author : Christopher Torng

sed -E -i.bak "s|(^.*PERIOD.*COND.*STOV.*negedge\sCLK.*$)|// commented \1|" $1

