module SyncSpRamBeNx64 #(
  parameter ADDR_WIDTH = 10  ,
  parameter DATA_DEPTH = 1024, // usually 2**ADDR_WIDTH, but can be lower
  parameter OUT_REGS   = 0   , // set to 1 to enable outregs
  parameter SIM_INIT   = 0     // will not be synthesized
                               // 0: no init, 1: zero init, 2: random init, 3: deadbeef init
                               // note: on verilator, 2 is not supported. define the VERILATOR macro to work around.
) (
  input  logic                  Clk_CI   ,
  input  logic                  Rst_RBI  ,
  input  logic                  CSel_SI  ,
  input  logic                  WrEn_SI  ,
  input  logic [           7:0] BEn_SI   ,
  input  logic [          63:0] WrData_DI,
  input  logic [ADDR_WIDTH-1:0] Addr_DI  ,
  output logic [          63:0] RdData_DO
);

// WARNING: This module (SyncSpRamBeNx64) is only expected to be instantiated
// with the following parameters in this design

// SyncSpRamBeNx64_00000008_00000100_0_2

  if (ADDR_WIDTH == 16'h8 && DATA_DEPTH == 16'h100)
    begin: macro


  freepdk45_1rf_lg8_w64_all mem(
  // Port 0: RW
      .clk0(Clk_CI),
      .csb0(~CSel_SI),
      .web0(~WrEn_SI),
      .ADDR0(Addr_DI),
      .DIN0(WrData_DI),
      .DOUT0(RdData_DO)
    );

    end // block: macro

endmodule // SyncSpRamBeNx64
