set NETLIST_SOURCE_FILES ""

# File order matters
set SVERILOG_SOURCE_FILES "
../coyote/src/verilog/Top.Bsg1AccelVLSIConfig.v
../coyote/src/verilog/bsg_1_to_n_tagged.v
../coyote/src/verilog/bsg_1_to_n_tagged_fifo.v
../coyote/src/verilog/bsg_channel_tunnel.v
../coyote/src/verilog/bsg_channel_tunnel_in.v
../coyote/src/verilog/bsg_channel_tunnel_out.v
../coyote/src/verilog/bsg_circular_ptr.v
../coyote/src/verilog/bsg_counter_clear_up.v
../coyote/src/verilog/bsg_counter_up_down_variable.v
../coyote/src/verilog/bsg_crossbar_o_by_i.v
../coyote/src/verilog/bsg_decode.v
../coyote/src/verilog/bsg_decode_with_v.v
../coyote/src/verilog/bsg_fifo_1r1w_pseudo_large.v
../coyote/src/verilog/bsg_fifo_1rw_large.v
../coyote/src/verilog/bsg_host.v
../coyote/src/verilog/bsg_mem_1r1w.v
../coyote/src/verilog/bsg_mem_1r1w_sync_mask_write_bit.v
../coyote/src/verilog/bsg_mem_1r1w_synth.v
../coyote/src/verilog/bsg_mem_1rw_sync.v
../coyote/src/verilog/bsg_mem_1rw_sync_mask_write_bit.v
../coyote/src/verilog/bsg_mux_one_hot.v
../coyote/src/verilog/bsg_nasti_client.v
../coyote/src/verilog/bsg_nasti_client_req.v
../coyote/src/verilog/bsg_nasti_client_resp.v
../coyote/src/verilog/bsg_rocket_node_client_rocc.v
../coyote/src/verilog/bsg_rocket_to_fsb_rocc.v
../coyote/src/verilog/bsg_round_robin_arb.v
../coyote/src/verilog/bsg_round_robin_n_to_1.v
../coyote/src/verilog/bsg_two_fifo.v
../coyote/src/verilog/sram.v


$::env(DESIGN_ROOT_DIR)/src/verilog/coyote_openroad.v
"
