##################################################################################
#                           POST-SIGNOFF PLUG-IN
##################################################################################
#
# This plug-in script is called after timeDesign -signoff from the run_signoff.tcl
# flow script.
#
##################################################################################
source $::env(INNOVUS_FLOW_DIR)/common/tcl/saveSignoffFiles.tcl

get_metric > $::env(INNOVUS_RESULTS_DIR)/post_$vars(step).metrics.txt
