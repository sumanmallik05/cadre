################################################################################
#                          PRE-PLACE PLUG-IN
################################################################################
#
# This plug-in script is called before placeDesign from the run_place.tcl flow
# script.
#
################################################################################
# Example tasks include:
#          - Power planning related tasks which includes
#            - Power planning for power domains (ring/strap creations)
#            - Power Shut-off cell power hookup
################################################################################


sroute -nets "VDD VSS" -connect corePin -corePinTarget none

addRing -nets "VDD VSS" -type core_rings -follow core \
        -layer {top M8 bottom M8 left M9 right M9} \
        -width $p_rng_w -spacing $p_rng_s \
        -extend_corner {tl tr bl br lt lb rt rb} \
        -offset $p_rng_s
         # -center 1

addStripe -set_to_set_distance $p_str_p \
          -spacing $p_str_s \
          -layer M5 \
          -width $p_str_w \
          -nets {VDD VSS} \
          -stacked_via_bottom_layer M1


################################################################################
# POWER CONFIGURATION PARAMETERS
################################################################################
# Lower and upper grid has already been done by PGA or simplePowerPlan.tcl
