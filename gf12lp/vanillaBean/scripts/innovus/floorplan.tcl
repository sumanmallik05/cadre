####################################################################################
#                             FLOORPLAN SCRIPT
####################################################################################

floorPlan -s $core_width \
             $core_height \
             $core_margin_l \
             $core_margin_b \
             $core_margin_r \
             $core_margin_t

setFlipping s

################################################################################
# MACRO PLACEMENT
################################################################################
# Mimimum memory spacings from ARM sram_sp_hde README

# Minimum memory to stdcell spacing
set min_mem_stdcell_spacing_h 0.540
set min_mem_stdcell_spacing_v 0.672

# Minimum memory to memory spacing
# NOTE: identical memory macros can be abutted on the non-pin edge
set min_mem_mem_spacing_h     1.440
set min_mem_mem_spacing_v     0.672

# Define cell sizes
set mem_x [dbGet [dbGetCellByName gf12lp_1rw_lg10_w32_all].size_x]
set mem_y [dbGet [dbGetCellByName gf12lp_1rw_lg10_w32_all].size_y]

set rf_x  [dbGet [dbGetCellByName gf12lp_2rf_lg5_w32_all].size_x]
set rf_y  [dbGet [dbGetCellByName gf12lp_2rf_lg5_w32_all].size_y]

# Macro instance list
set mem "[dbGet [dbGet -p2 top.insts.cell.name gf12lp_1rw_lg10_w32_all].name]
         [dbGet [dbGet -p2 top.insts.cell.name gf12lp_1rw_lg10_w32_byte].name]
         [dbGet [dbGet -p2 top.insts.cell.name gf12lp_2rf_lg5_w32_all].name]"

# Place instances
set spacing 0

placeInstance [lindex $mem 0] [expr $core_margin_l + $spacing] [expr $core_margin_b + $spacing*$t_pitch] MY
placeInstance [lindex $mem 1] [expr $core_width + $core_margin_l - $mem_x - $spacing] [expr $core_margin_b + $spacing*$t_pitch]
placeInstance [lindex $mem 2] [expr $core_margin_l + $mem_x + $min_mem_mem_spacing_h + 2*$spacing] \
                              [expr $core_margin_b + $mem_y - $rf_y + $spacing*$t_pitch] MY

# Style 1: RFs on opposite sides
#placeInstance [lindex $mem 3] [expr $core_margin_l + $core_width - $mem_x - (2 * $p_str_w) - (3 * $p_str_s) - $rf_x] \
#                              [expr $core_height + $core_margin_b - $rf_y]

# Style 2: RFs together in UL
placeInstance [lindex $mem 3] [expr $core_margin_l + $mem_x + (2 * $min_mem_mem_spacing_h) + $rf_x + 3*$spacing] \
                              [expr $core_margin_b + $mem_y - $rf_y + $spacing*$t_pitch] MY

################################################################################
# I/O PIN PLACEMENT
################################################################################

# Create pin lists
selectPin link_out*
set link_out_pins [dbGet selected.name]
deselectAll
selectPin link_in*
set link_in_pins  [dbGet selected.name]
deselectAll

# Get number of link_in/link_out pins per side
set num_links [expr [llength $link_out_pins] / 4]

# Set side and pin indicies based on pin ordering
set sides  {      LEFT         RIGHT      TOP         BOTTOM     }
set dir    {counterclockwise clockwise clockwise counterclockwise}
set layers {       M6           M6        M5            M5       }

# Spread pins along each side according to processor link direction
for {set side_idx 0} {$side_idx < 4} {incr side_idx} {
    set start_idx [expr ($side_idx  ) * $num_links    ]
    set end_idx   [expr ($side_idx+1) * $num_links - 1]

    set in_subset  [lrange $link_in_pins  $start_idx $end_idx]
    set out_subset [lrange $link_out_pins $start_idx $end_idx]

    # clear for this iteration
    unset -nocomplain side_subset

    foreach in_element $in_subset out_element $out_subset {
        # Need to make sure the in/out order is flipped on opposite sides so that pins line up when abutted
        if {$side_idx == 1 || $side_idx == 3} {
            lappend side_subset $in_element $out_element
        } else {
            lappend side_subset $out_element $in_element
        }
    }

    if {$side_idx <= 1} {
        editPin -pin $side_subset -fixedPin 1 -layer [lindex $layers $side_idx] \
                -snap TRACK -side [lindex $sides $side_idx] -spreadDirection [lindex $dir $side_idx] -spreadType SIDE -use SIGNAL
    } else {
        editPin -pin $side_subset -fixedPin 1 -layer [lindex $layers $side_idx] \
                -snap TRACK -side [lindex $sides $side_idx] -spreadDirection [lindex $dir $side_idx] -spreadType RANGE -start "$mem_x 0" -end "[expr $die_width - $mem_x]" 0 -use SIGNAL
    }
}

# Assign locations for other pins
selectPin "my_* reset_i"
set side_subset [dbGet selected.name]

editPin -pin $side_subset -fixedPin 1 -layer M7 -snap TRACK -side BOTTOM -spreadType CENTER -use SIGNAL -spacing 16 -unit TRACK

editPin -pin "clk_i" -fixedPin 1 -layer M7 -snap TRACK -side BOTTOM -spreadType START -use CLOCK -start "[expr $die_width / 2 + 8] 0"
