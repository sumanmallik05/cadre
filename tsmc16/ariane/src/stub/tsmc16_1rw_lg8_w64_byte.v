module tsmc16_1rw_lg8_w64_byte (Q, CLK, CEN, GWEN, A, D, WEN, STOV, EMA, EMAW, EMAS,
    RET1N);

  parameter ASSERT_PREFIX = "";
  parameter BITS = 64;
  parameter WORDS = 256;
  parameter MUX = 2;
  parameter MEM_WIDTH = 128; // redun block size 2, 64 on left, 64 on right
  parameter MEM_HEIGHT = 128;
  parameter WP_SIZE = 1 ;
  parameter UPM_WIDTH = 3;
  parameter UPMW_WIDTH = 2;
  parameter UPMS_WIDTH = 1;
  parameter ROWS = 128;

  output [63:0] Q;
  input  CLK;
  input  CEN;
  input  GWEN;
  input [7:0] A;
  input [63:0] D;
  input [63:0] WEN;
  input  STOV;
  input [2:0] EMA;
  input [1:0] EMAW;
  input  EMAS;
  input  RET1N;


endmodule
