module tsmc16_1rw_lg10_w32_byte (Q, CLK, CEN, GWEN, A, D, WEN, STOV, EMA, EMAW, EMAS,
    RET1N);

  parameter ASSERT_PREFIX = "";
  parameter BITS = 32;
  parameter WORDS = 1024;
  parameter MUX = 4;
  parameter MEM_WIDTH = 128; // redun block size 4, 64 on left, 64 on right
  parameter MEM_HEIGHT = 256;
  parameter WP_SIZE = 1 ;
  parameter UPM_WIDTH = 3;
  parameter UPMW_WIDTH = 2;
  parameter UPMS_WIDTH = 1;
  parameter ROWS = 256;

  output [31:0] Q;
  input  CLK;
  input  CEN;
  input  GWEN;
  input [9:0] A;
  input [31:0] D;
  input [31:0] WEN;
  input  STOV;
  input [2:0] EMA;
  input [1:0] EMAW;
  input  EMAS;
  input  RET1N;

endmodule
