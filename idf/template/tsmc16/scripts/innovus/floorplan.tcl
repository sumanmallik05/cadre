####################################################################################
#                             FLOORPLAN SCRIPT
####################################################################################

floorPlan -s $core_width \
             $core_height \
             $core_margin_l \
             $core_margin_b \
             $core_margin_r \
             $core_margin_t

setFlipping s

################################################################################
# MACRO PLACEMENT
################################################################################

if {$::env(IDF_FLOORPLAN) == "CUSTOM"} {
  source $::env(INNOVUS_SCRIPTS_DIR)/customPlacement.tcl
} elseif {$::env(IDF_FLOORPLAN) == "IDF"} {
@@@MACRO_PLACEMENT@@@
} else {
  source $::env(INNOVUS_FLOW_DIR)/common/tcl/planDesign.tcl
}


@@@PLACEMENT_GUIDE@@@

################################################################################
# I/O PIN PLACEMENT
################################################################################

@@@IO_PIN_PLACEMENT@@@

zoomBox [dbGet top.fPlan.box_llx] [dbGet top.fPlan.box_lly] [dbGet top.fPlan.box_urx] [dbGet top.fPlan.box_ury]
source $::env(INNOVUS_FLOW_DIR)/common/tcl/gui.color.tcl
createSnapshot -dir snapshot -name $vars(step) -overwrite
