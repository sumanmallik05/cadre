
if {[info exists ::env(CLOCK_PERIOD)]} {
  set clock_cycle $::env(CLOCK_PERIOD)
} else {
  set clock_cycle 2
}

set uncertainty 0.0
set io_delay 0.0
set clock_port clk

create_clock -name "clk" -add -period $clock_cycle [get_ports $clock_port]
set_clock_uncertainty $uncertainty  [get_clocks clk]

set_input_delay -clock [get_clocks clk] -add_delay -max $io_delay [all_inputs]
set_output_delay -clock [get_clocks clk] -add_delay -max $io_delay [all_outputs]
