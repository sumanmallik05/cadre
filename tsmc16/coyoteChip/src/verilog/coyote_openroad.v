module coyote_openroad (
  inout wire [15:0] PAD_N,
  inout wire [15:0] PAD_E,
  inout wire [15:0] PAD_W,
  inout wire [15:0] PAD_S
);

  wire [15:0] PBIDIRN_N_A ;
  wire [15:0] PBIDIRN_N_OE;
  wire [15:0] PBIDIRN_N_IE;
  wire [15:0] PBIDIRN_N_Y ;

  wire [15:0] PBIDIRN_E_A ;
  wire [15:0] PBIDIRN_E_OE;
  wire [15:0] PBIDIRN_E_IE;
  wire [15:0] PBIDIRN_E_Y ;

  wire [15:0] PBIDIRN_S_A ;
  wire [15:0] PBIDIRN_S_OE;
  wire [15:0] PBIDIRN_S_IE;
  wire [15:0] PBIDIRN_S_Y ;

  wire [15:0] PBIDIRN_W_A ;
  wire [15:0] PBIDIRN_W_OE;
  wire [15:0] PBIDIRN_W_IE;
  wire [15:0] PBIDIRN_W_Y ;


  PBIDIRN_18_18_FS_DR_V PBIDIRN_N[15:0] ( .A(PBIDIRN_N_A), .DS0(1'b0), .DS1(1'b0), .IE(PBIDIRN_N_IE),
    .IS(1'b0), .OE(PBIDIRN_N_OE), .PE(1'b0), .POE(1'b0), .PS(1'b0), .RTO(1'b1),
    .SNS(1'b1), .SR(1'b0), .PAD(PAD_N), .Y(PBIDIRN_N_Y) );

  PBIDIRN_18_18_FS_DR_H PBIDIRN_E[15:0] ( .A(PBIDIRN_E_A), .DS0(1'b0), .DS1(1'b0), .IE(PBIDIRN_E_IE),
    .IS(1'b0), .OE(PBIDIRN_E_OE), .PE(1'b0), .POE(1'b0), .PS(1'b0), .RTO(1'b1),
    .SNS(1'b1), .SR(1'b0), .PAD(PAD_E), .Y(PBIDIRN_E_Y) );

  PBIDIRN_18_18_FS_DR_H PBIDIRN_W[15:0] ( .A(PBIDIRN_W_A), .DS0(1'b0), .DS1(1'b0), .IE(PBIDIRN_W_IE),
    .IS(1'b0), .OE(PBIDIRN_W_OE), .PE(1'b0), .POE(1'b0), .PS(1'b0), .RTO(1'b1),
    .SNS(1'b1), .SR(1'b0), .PAD(PAD_W), .Y(PBIDIRN_W_Y) );

  PBIDIRN_18_18_FS_DR_V PBIDIRN_S[15:0] ( .A(PBIDIRN_S_A), .DS0(1'b0), .DS1(1'b0), .IE(PBIDIRN_S_IE),
    .IS(1'b0), .OE(PBIDIRN_S_OE), .PE(1'b0), .POE(1'b0), .PS(1'b0), .RTO(1'b1),
    .SNS(1'b1), .SR(1'b0), .PAD(PAD_S), .Y(PBIDIRN_S_Y) );


  // east and west is input
  assign PBIDIRN_E_OE[7:0] = {8{1'b0}};
  assign PBIDIRN_E_IE[7:0] = {8{1'b1}};
  assign PBIDIRN_E_OE[15:8] = {8{1'b0}};
  assign PBIDIRN_E_IE[15:8] = {8{1'b1}};


  // north and west is input
  assign PBIDIRN_W_OE = {15{1'b0}};
  assign PBIDIRN_W_IE = {15{1'b1}};
  assign PBIDIRN_N_OE = {15{1'b0}};
  assign PBIDIRN_N_IE = {15{1'b1}};

  // south is output
  assign PBIDIRN_S_OE = {15{1'b1}};
  assign PBIDIRN_S_IE = {15{1'b0}};


  wire           clk_i               ;
  wire           reset_i             ;
  wire           en_i                ;
  wire           rocc_cmd_v_o        ;
  wire [160-1:0] rocc_cmd_data_o     ;
  wire           rocc_cmd_ready_i    ;
  wire           rocc_resp_v_i       ;
  wire [ 69-1:0] rocc_resp_data_i    ;
  wire           rocc_resp_ready_o   ;
  wire           rocc_mem_req_v_i    ;
  wire [123-1:0] rocc_mem_req_data_i ;
  wire           rocc_mem_req_ready_o;
  wire           rocc_mem_resp_v_o   ;
  wire [253-1:0] rocc_mem_resp_data_o;
  wire [  2-1:0] rocc_ctrl_i         ;
  wire [  3-1:0] rocc_ctrl_o         ;
  wire           fsb_node_v_i        ;
  wire [ 80-1:0] fsb_node_data_i     ;
  wire           fsb_node_ready_o    ;
  wire           fsb_node_v_o        ;
  wire [ 80-1:0] fsb_node_data_o     ;
  wire           fsb_node_yumi_i     ;

  assign clk_i            = PBIDIRN_E_Y[0];
  assign reset_i          = PBIDIRN_E_Y[1];
  assign en_i             = PBIDIRN_E_Y[2];
  assign rocc_cmd_ready_i = PBIDIRN_E_Y[3];
  assign rocc_resp_v_i    = PBIDIRN_E_Y[4];
  assign rocc_mem_req_v_i = PBIDIRN_E_Y[5];
  assign fsb_node_v_i     = PBIDIRN_E_Y[6];
  assign fsb_node_yumi_i  = PBIDIRN_E_Y[7];

  assign PBIDIRN_E_A[8]  = rocc_cmd_v_o;
  assign PBIDIRN_E_A[9]  = rocc_resp_ready_o;
  assign PBIDIRN_E_A[10] = rocc_mem_req_ready_o;
  assign PBIDIRN_E_A[11] = rocc_mem_resp_v_o;
  assign PBIDIRN_E_A[12] = fsb_node_ready_o;
  assign PBIDIRN_E_A[13] = fsb_node_v_o;

  assign rocc_resp_data_i    = {9{PBIDIRN_W_Y[7:0]}}; // 69
  assign rocc_mem_req_data_i = {16{PBIDIRN_W_Y[15:8]}}; // 123
  assign rocc_ctrl_i         = PBIDIRN_N_Y[1:0]; // 2
  assign fsb_node_data_i     = {10{PBIDIRN_N_Y[15:2]}}; // 80

  assign PBIDIRN_S_A[3:0]   = {^rocc_cmd_data_o[159:120],^rocc_cmd_data_o[119:80],^rocc_cmd_data_o[79:40],^rocc_cmd_data_o[39:0]}  ; // 160
  assign PBIDIRN_S_A[7:4]   = {^rocc_mem_resp_data_o[252:192],^rocc_mem_resp_data_o[191:128],^rocc_mem_resp_data_o[127:64],^rocc_mem_resp_data_o[63:0]} ; // 254
  assign PBIDIRN_S_A[11:8]  = {rocc_ctrl_o,0} ; // 3
  assign PBIDIRN_S_A[15:12] = {^fsb_node_data_o[79:60],^fsb_node_data_o[59:40],^fsb_node_data_o[39:20],^fsb_node_data_o[19:0]} ; // 80



  bsg_rocket_node_client_rocc clnt (
    .clk_i               (clk_i               ),
    // ctrl
    .reset_i             (reset_i             ),
    .en_i                (en_i                ),
    // rocc cmd out
    .rocc_cmd_v_o        (rocc_cmd_v_o        ),
    .rocc_cmd_data_o     (rocc_cmd_data_o     ),
    .rocc_cmd_ready_i    (rocc_cmd_ready_i    ),
    // rocc resp in
    .rocc_resp_v_i       (rocc_resp_v_i       ),
    .rocc_resp_data_i    (rocc_resp_data_i    ),
    .rocc_resp_ready_o   (rocc_resp_ready_o   ),
    // rocc mem req in
    .rocc_mem_req_v_i    (rocc_mem_req_v_i    ),
    .rocc_mem_req_data_i (rocc_mem_req_data_i ),
    .rocc_mem_req_ready_o(rocc_mem_req_ready_o),
    // rocc mem resp out
    .rocc_mem_resp_v_o   (rocc_mem_resp_v_o   ),
    .rocc_mem_resp_data_o(rocc_mem_resp_data_o),
    // rocc ctrl in
    .rocc_ctrl_i         (rocc_ctrl_i         ),
    // rocc ctrl out
    .rocc_ctrl_o         (rocc_ctrl_o         ),
    // fsb in
    .fsb_node_v_i        (fsb_node_v_i        ),
    .fsb_node_data_i     (fsb_node_data_i     ),
    .fsb_node_ready_o    (fsb_node_ready_o    ),
    // fsb out
    .fsb_node_v_o        (fsb_node_v_o        ),
    .fsb_node_data_o     (fsb_node_data_o     ),
    .fsb_node_yumi_i     (fsb_node_yumi_i     )
  );

endmodule
