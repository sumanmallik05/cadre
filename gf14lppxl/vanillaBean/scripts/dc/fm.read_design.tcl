puts "Info: Running script [info script]\n"

# the following script change the generic include folders with
# tech-dependant ones
source ./scripts/dc/$DESIGN_NAME.include.tcl
set_app_var search_path "${SVERILOG_INCLUDE_PATHS} $search_path"

# The following script change generic filelist with tech-dependent one
source ./scripts/dc/$DESIGN_NAME.filelist.tcl

puts "Info: Reading the following System Verilog files as Formality reference design.\n"
# Print one file per line
redirect -tee sverilog_filelist_fm.txt {puts "[join $SVERILOG_SOURCE_FILES \n]\n"}

puts "Info: Number of System Verilog files: [llength $SVERILOG_SOURCE_FILES]\n"

read_sverilog -r -libname $FM_LIB_NAME -define {ASIC SYNTHESIS_HARDWARE NO_DUMMY} $SVERILOG_SOURCE_FILES
read_sverilog -r -libname $FM_LIB_NAME -define {ASIC SYNTHESIS_HARDWARE NO_DUMMY} $NETLIST_SOURCE_FILES

puts "Info: Completed script [info script]\n"
