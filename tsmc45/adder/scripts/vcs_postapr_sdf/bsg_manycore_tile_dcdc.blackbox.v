module bsg_manycore_tile_1024_1_1024_1_3_32_20_0_00000000_00000000_0 (
	clk_i, 
	reset_i, 
	link_in, 
	link_out, 
	my_x_i, 
	my_y_i);
   input clk_i;
   input reset_i;
   input [295:0] link_in;
   output [295:0] link_out;
   input [0:0] my_x_i;
   input [2:0] my_y_i;
endmodule
