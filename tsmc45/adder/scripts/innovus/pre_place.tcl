#################################################################################
##                          PRE-PLACE PLUG-IN
#################################################################################
##
## This plug-in script is called before placeDesign from the run_place.tcl flow
## script.
##
#################################################################################
## Example tasks include:
##          - Power planning related tasks which includes
##            - Power planning for power domains (ring/strap creations)
##            - Power Shut-off cell power hookup
#################################################################################
#
#if {([string tolower $vars(use_pga)] != "true")} {
#    source $::env(INNOVUS_FLOW_DIR)/common/tcl/simplePowerPlan.tcl
#}
#################################################################################
## POWER CONFIGURATION PARAMETERS
#################################################################################
## Lower grid has already been done by PGA or simplePowerPlan.tcl
## Only need to do upper grid on M8 and M9
## Also need to do memory M5 grid until we can figure why PGA isn't doing that
#
## Power mesh configuration according to ARM recommendations
#
#### CONFIGURABLE PARAMETERS ####################################################
#
## M5 Mesh for memories
#set M5_str_width   0.960    ;# Minimum width recommended by ARM is 0.120
#
## M8 and M9 course-grain mesh
#set M8_str_pitch   20.000   ;
##set M8_str_spacing 10       ;# Number of routable M8 tracks between stripes. This value controls stripe density
#set M9_str_pitch   16.000   ;
##set M9_str_spacing 50       ;# Number of routable M9 tracks between stripes. This value controls stripe density
#
#### NON-CONFIGURABLE PARAMETERS - DO NOT CHANGE ################################
## M2 and M3 fine-grain mesh
#set M2_str_pitch   $t_pitch ;# Mandated by ARM - do not change
#set M2_str_width   0.076    ;# Mandated by ARM - do not change
#set M2_str_spacing 2        ;# Mandated by ARM - do not change
#
#set M3_track_pitch [dbGet [dbGet -p1 head.layers.num 3].pitchX]
#
## M5 Mesh for memories
#set M5_t_pitch     0.040    ;
#set M5_str_pitch   9.000    ;# Mandated by ARM - do not change
#set M5_str_spacing 2        ;
#set M5_str_set_spacing  [expr ($M5_str_pitch - 2*$M5_str_width) / 2]
#
## M8 and M9 course-grain mesh
## ARM recommends that M8 is 8x the width of the M2/M3 grid and M9 is 16x
#set M8_t_pitch     0.360    ;
#set M8_str_width   2.4
#set M8_str_spacing 2        ;
#set M8_str_set_spacing  [expr ($M8_str_pitch - 2*$M8_str_width) / 2]
#
#set M9_t_pitch     0.360    ;
#set M9_str_width   3.6      ;#This is to make it wide enough for the AP to drop down onto it
#set M9_str_spacing 2        ;
#set M9_str_set_spacing  [expr ($M9_str_pitch - 2*$M9_str_width) / 2]
#
#################################################################################
## POWER MESH CREATION
#################################################################################
#
#setViaGenMode -reset
#setViaGenMode -viarule_preference default
#setViaGenMode -ignore_DRC 0
#
#setAddStripeMode -reset
#setAddStripeMode -stacked_via_bottom_layer M4 -stacked_via_top_layer M5 -split_vias 1
#
## TODO: temporary - create a core ring for connectivity
#addRing -nets {VDD VSS} -type core_rings -follow core \
#    -layer {top M8 bottom M8 left M9 right M9} \
#    -width $p_rng_w -spacing $p_rng_s -offset $p_rng_s \
#    -extend_corner {tl tr bl br lt lb rt rb}
#
#set mem ""
#### M5 mesh over memory ###
#foreach block $mem {
#    selectInst $block
#    addStripe -nets {VSS VDD} -layer M5 -direction vertical   \
#        -width $M5_str_width                                  \
#        -spacing $M5_str_set_spacing                          \
#        -set_to_set_distance $M5_str_pitch                    \
#        -start_offset 2                                       \
#        -stop_offset 2                                        \
#        -area [dbGet selected.box]
#    deselectAll
#}
#
#### M8 M9 mesh ###
#
## TODO: the width and spacing of these metals are just placeholders
#setAddStripeMode -reset
#setAddStripeMode -stacked_via_bottom_layer M5 -stacked_via_top_layer M9
#
#addStripe -nets {VSS VDD} -layer M8 -direction horizontal \
#    -width $M8_str_width                                  \
#    -spacing $M8_str_set_spacing                          \
#    -set_to_set_distance $M8_str_pitch                    \
#    -start [expr $core_margin_l + $p_rng_w]               \
#    -extend_to design_boundary
#
#setAddStripeMode -reset
#setAddStripeMode -stacked_via_bottom_layer M8 -stacked_via_top_layer M9
#
#addStripe -nets {VSS VDD} -layer M9 -direction vertical \
#    -width $M9_str_width                                \
#    -spacing $M9_str_set_spacing                        \
#    -set_to_set_distance $M9_str_pitch                  \
#    -start [expr $core_margin_l + $p_rng_w]             \
#    -extend_to design_boundary
#
#setViaGenMode -reset
#setViaGenMode -viarule_preference default
#setViaGenMode -ignore_DRC 0
#
## select the nets to via between
#editSelect -layers {M4 M9} -net VDD -type Special -wires_only 1
#editPowerVia -between_selected_wires true -add_vias 1 \
#    -orthogonal_only 0 -split_vias 1
#deselectAll
#
## select the nets to via between
#editSelect -layers {M4 M9} -net VSS -type Special -wires_only 1
#editPowerVia -between_selected_wires true -add_vias 1 \
#    -orthogonal_only 0 -split_vias 1
#deselectAll
