`ifdef BSG_PACKAGING
    `include "bsg_padmapping.v"
    `include "bsg_iopad_macros.v"
`endif

module adder

`ifdef BSG_PACKAGING
    // pull in BSG Two's top-level module signature, and the definition of the pads
    `include "bsg_pinout.v"
    `include "bsg_iopads.v"
`else
    // Block-level I/O
    (
        input  logic [1:0] p_sdi_sclk_i,
        input  logic [7:0] p_sdi_A_data_i, p_sdi_B_data_i,
        output logic [7:0] p_sdo_A_data_o
    );
`endif

logic [7:0] op1, op2, out;

assign op1 = p_sdi_A_data_i;
assign op2 = p_sdi_B_data_i;
assign p_sdo_A_data_o = out;


always_ff @(posedge p_sdi_sclk_i[0]) begin
    out <= op1 + op2;
end


`ifdef BSG_PACKAGING
    `include "bsg_pinout_end.v"
`else
    endmodule
`endif
