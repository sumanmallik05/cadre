module bsg_manycore_tile (
	clk_i, 
	reset_i, 
	link_in, 
	link_out, 
	my_x_i, 
	my_y_i);
   input clk_i;
   input reset_i;
   input [355:0] link_in;
   output [355:0] link_out;
   input [3:0] my_x_i;
   input [4:0] my_y_i;
endmodule
