puts "Info: Running script [info script]\n"

custom_report > ${DC_REPORTS_DIR}/${DESIGN_NAME}.mapped.custom_report.rpt

# mostly for seeing if multicore is on
report_host_options

puts "Info: Completed script [info script]\n"
