####################################################################################
#                              ALWAYS_SOURCE PLUG-IN
#####################################################################################
#
# This plug-in script is called from all flow scripts after loading the setup.tcl
# but after to loading the design data.  It can be used to set variables that affect
# non-persistent information
#
#####################################################################################
set t_pitch [dbGet top.fPlan.coreSite.size_y]     ;# Pitch between power rails (standard cell height)
# set f_pitch [dbGet head.finGridPitch]             ;# Pitch between fins

# Variables used by Power Planning scripts
set pwr_net_list {VDD VSS}              ;# List of Power nets

set p_rng_w      0.960                  ;# Power ring metal width
set p_rng_s      0.480                  ;# Power ring metal space
set p_str_w      0.720                  ;# Power stripe metal width
set p_str_s      0.360                  ;# Power stripe metal space
set p_str_p      8.000                  ;# Power stripe metal space


# Floorplan Variables
set p_str_margin  [expr ([llength $pwr_net_list] * ($p_str_w + $p_str_s)) + $p_str_s]
set core_margin_t [expr ([llength $pwr_net_list] * ($p_rng_w + $p_rng_s)) + $p_rng_s]
set core_margin_b [expr ([llength $pwr_net_list] * ($p_rng_w + $p_rng_s)) + $p_rng_s]
set core_margin_r [expr ([llength $pwr_net_list] * ($p_rng_w + $p_rng_s)) + $p_rng_s]
set core_margin_l [expr ([llength $pwr_net_list] * ($p_rng_w + $p_rng_s)) + $p_rng_s]

set core_width   [expr 50 * $t_pitch] ;# Core Area Width
set core_height  [expr 50 * $t_pitch] ;# Core Area Height

