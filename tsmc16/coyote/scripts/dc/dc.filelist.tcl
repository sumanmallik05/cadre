set NETLIST_SOURCE_FILES ""

# File order matters
set SVERILOG_SOURCE_FILES "
$::env(DESIGN_ROOT_DIR)/src/verilog/Top.Bsg1AccelVLSIConfig.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_1_to_n_tagged.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_1_to_n_tagged_fifo.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_channel_tunnel.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_channel_tunnel_in.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_channel_tunnel_out.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_circular_ptr.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_counter_clear_up.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_counter_up_down_variable.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_crossbar_o_by_i.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_decode.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_decode_with_v.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_fifo_1r1w_pseudo_large.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_fifo_1rw_large.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_host.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_mem_1r1w.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_mem_1r1w_sync_mask_write_bit.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_mem_1r1w_synth.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_mem_1rw_sync.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_mem_1rw_sync_mask_write_bit.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_mux_one_hot.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_nasti_client.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_nasti_client_req.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_nasti_client_resp.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_rocket_node_client_rocc.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_rocket_to_fsb_rocc.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_round_robin_arb.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_round_robin_n_to_1.v
$::env(DESIGN_ROOT_DIR)/src/verilog/bsg_two_fifo.v
$::env(DESIGN_ROOT_DIR)/src/verilog/sram.v
"
