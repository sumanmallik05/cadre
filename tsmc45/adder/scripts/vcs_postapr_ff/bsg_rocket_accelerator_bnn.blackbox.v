module bsg_rocket_accelerator_bnn (
	clk_i, 
	reset_i, 
	en_i, 
	rocc_cmd_v_i, 
	rocc_cmd_data_i, 
	rocc_cmd_ready_o, 
	rocc_resp_v_o, 
	rocc_resp_data_o, 
	rocc_resp_ready_i, 
	rocc_mem_req_v_o, 
	rocc_mem_req_data_o, 
	rocc_mem_req_ready_i, 
	rocc_mem_resp_v_i, 
	rocc_mem_resp_data_i, 
	rocc_ctrl_o, 
	rocc_ctrl_i, 
	manycore_req_msg_i, 
	manycore_req_rdy_o, 
	manycore_req_val_i);
   input clk_i;
   input reset_i;
   input en_i;
   input rocc_cmd_v_i;
   input [159:0] rocc_cmd_data_i;
   output rocc_cmd_ready_o;
   output rocc_resp_v_o;
   output [68:0] rocc_resp_data_o;
   input rocc_resp_ready_i;
   output rocc_mem_req_v_o;
   output [122:0] rocc_mem_req_data_o;
   input rocc_mem_req_ready_i;
   input rocc_mem_resp_v_i;
   input [252:0] rocc_mem_resp_data_i;
   output [1:0] rocc_ctrl_o;
   input [2:0] rocc_ctrl_i;
   input [63:0] manycore_req_msg_i;
   output [0:0] manycore_req_rdy_o;
   input [0:0] manycore_req_val_i;

endmodule
