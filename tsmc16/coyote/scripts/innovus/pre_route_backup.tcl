add_ndr -spacing_multiplier {M3:M7 2} \
        -name siWires

editSelect -net {r2f/rocket/RocketTile/icache/icache/s1_dout_*[*] }
#editSelect -net {r2f/rocket/RocketTile/dcache/T159[*] r2f/rocket/RocketTile/dcache/T176[*] r2f/rocket/RocketTile/dcache/T190[*] r2f/rocket/RocketTile/dcache/T203[*] r2f/rocket/RocketTile/dcache/data/T3[*] r2f/rocket/RocketTile/dcache/data/T4[*] r2f/rocket/RocketTile/icache/icache/s1_dout_*[*] }
foreach net [dbGet selected.net.name] {
  setAttribute -net $net \
               -non_default_rule siWires \
               -bottom_preferred_routing_layer 3 \
               -top_preferred_routing_layer 7 \
               -preferred_routing_layer_effort hard
}

deselectAll
