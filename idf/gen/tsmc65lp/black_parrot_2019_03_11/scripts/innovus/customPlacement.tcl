set margin 3 ;# Set specifically to avoid dangling stripes in the margin
set spacing 4
set bigSpace 112

# Lower Left Corner
# ##############################################################################

placeInstance multi_top/rof1_0__core/fe/icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem \
  [expr $margin + [dbGet top.fplan.coreBox_llx]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90
placeInstance multi_top/rof1_0__core/fe/icache_1/data_mem_banks_5__data_mem_bank/macro_mem/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem].box_urx]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90
placeInstance multi_top/rof1_0__core/fe/icache_1/data_mem_banks_6__data_mem_bank/macro_mem/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/icache_1/data_mem_banks_5__data_mem_bank/macro_mem/mem].box_urx]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90
placeInstance multi_top/rof1_0__core/fe/icache_1/data_mem_banks_7__data_mem_bank/macro_mem/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/icache_1/data_mem_banks_6__data_mem_bank/macro_mem/mem].box_urx]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90
placeInstance multi_top/rof1_0__core/fe/icache_1/data_mem_banks_0__data_mem_bank/macro_mem/mem \
  [expr $margin + [dbGet top.fplan.coreBox_llx]] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem].box_ury]] \
  R90
placeInstance multi_top/rof1_0__core/fe/icache_1/data_mem_banks_1__data_mem_bank/macro_mem/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem].box_urx]] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem].box_ury]] \
  R90
placeInstance multi_top/rof1_0__core/fe/icache_1/data_mem_banks_2__data_mem_bank/macro_mem/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/icache_1/data_mem_banks_5__data_mem_bank/macro_mem/mem].box_urx]] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem].box_ury]] \
  R90
placeInstance multi_top/rof1_0__core/fe/icache_1/data_mem_banks_3__data_mem_bank/macro_mem/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/icache_1/data_mem_banks_6__data_mem_bank/macro_mem/mem].box_urx]] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/icache_1/data_mem_banks_4__data_mem_bank/macro_mem/mem].box_ury]] \
  R90


# Lower Right Corner
# ##############################################################################
placeInstance multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_7__data_mem/macro_mem/mem \
  [expr [dbGet top.fPlan.coreBox_urx] - [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_7__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90
placeInstance multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_6__data_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_7__data_mem/macro_mem/mem].box_llx] - [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_6__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90
placeInstance multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_5__data_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_6__data_mem/macro_mem/mem].box_llx] - [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_5__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90
placeInstance multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_4__data_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_5__data_mem/macro_mem/mem].box_llx] - [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_4__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90

placeInstance multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_3__data_mem/macro_mem/mem \
  [expr [dbGet top.fPlan.coreBox_urx] - [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_3__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_7__data_mem/macro_mem/mem].box_ury]] \
  MX90
placeInstance multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_2__data_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_3__data_mem/macro_mem/mem].box_llx] - [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_2__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_6__data_mem/macro_mem/mem].box_ury]] \
  MX90
placeInstance multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_1__data_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_2__data_mem/macro_mem/mem].box_llx] - [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_1__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_5__data_mem/macro_mem/mem].box_ury]] \
  MX90
placeInstance multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_0__data_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_1__data_mem/macro_mem/mem].box_llx] - [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_0__data_mem/macro_mem/mem].box_sizey] - $spacing] \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/data_mem_4__data_mem/macro_mem/mem].box_ury]] \
  MX90

# Lower Middle
# ##############################################################################

placeInstance multi_top/rof1_0__core/be/be_mmu/dcache/tag_mem/macro_mem/mem \
  [expr [dbGet top.fPlan.coreBox_llx] + ([dbGet top.fPlan.coreBox_sizex]/2) + ($bigSpace/2)] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90
placeInstance multi_top/rof1_0__core/be/be_mmu/dcache/stat_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/be/be_mmu/dcache/tag_mem/macro_mem/mem].box_urx] + $spacing] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90


placeInstance multi_top/rof1_0__core/fe/bp_fe_pc_gen_1/genblk1_branch_prediction_1/btb_1/btb_mem/macro_mem/mem \
  [expr [dbGet top.fPlan.coreBox_llx] + ([dbGet top.fPlan.coreBox_sizex]/2) - ($bigSpace/2) - [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/bp_fe_pc_gen_1/genblk1_branch_prediction_1/btb_1/btb_mem/macro_mem/mem].box_sizey]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90
placeInstance multi_top/rof1_0__core/fe/icache_1/tag_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/bp_fe_pc_gen_1/genblk1_branch_prediction_1/btb_1/btb_mem/macro_mem/mem].box_llx] - $spacing - [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/icache_1/tag_mem/macro_mem/mem].box_sizey]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90
placeInstance multi_top/rof1_0__core/fe/icache_1/metadata_mem/macro_mem/mem \
  [expr [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/icache_1/tag_mem/macro_mem/mem].box_llx] - $spacing - [dbGet [dbGet -p1 top.insts.name multi_top/rof1_0__core/fe/icache_1/metadata_mem/macro_mem/mem].box_sizey]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  MX90

# Upper Middle
# ##############################################################################

placeInstance multi_top/me/genblk1_0__bp_cce_top/bp_cce/directory/wg_ram/macro_mem0/mem \
  [expr [dbGet top.fPlan.coreBox_llx] + ([dbGet top.fPlan.coreBox_sizex]/2) + $bigSpace] \
  [expr [dbGet top.fPlan.coreBox_ury] - $margin - [dbGet [dbGet -p1 top.insts.name multi_top/me/genblk1_0__bp_cce_top/bp_cce/directory/wg_ram/macro_mem0/mem].box_sizex]] \
  R270
placeInstance multi_top/me/genblk1_0__bp_cce_top/bp_cce/directory/wg_ram/macro_mem1/mem \
  [expr [dbGet [dbGet -p1 top.insts.name multi_top/me/genblk1_0__bp_cce_top/bp_cce/directory/wg_ram/macro_mem0/mem].box_urx] + $spacing] \
  [expr [dbGet top.fPlan.coreBox_ury] - $margin - [dbGet [dbGet -p1 top.insts.name multi_top/me/genblk1_0__bp_cce_top/bp_cce/directory/wg_ram/macro_mem1/mem].box_sizex]] \
  R270

placeInstance multi_top/me/genblk1_0__bp_cce_top/bp_cce/pc_inst_ram/cce_inst_ram/macro_mem/mem \
  [expr [dbGet top.fPlan.coreBox_llx] + ([dbGet top.fPlan.coreBox_sizex]/2) - $bigSpace - [dbGet [dbGet -p1 top.insts.name multi_top/me/genblk1_0__bp_cce_top/bp_cce/pc_inst_ram/cce_inst_ram/macro_mem/mem].box_sizex]] \
  [expr [dbGet top.fPlan.coreBox_ury] - $margin - [dbGet [dbGet -p1 top.insts.name multi_top/me/genblk1_0__bp_cce_top/bp_cce/pc_inst_ram/cce_inst_ram/macro_mem/mem].box_sizex]] \
  R270

selectInst [join "[dbGet -e [dbGet -p2 top.insts.cell.name $::env(PLATFORM)_*].name]"]
cutRow -selected -leftGap  $spacing -rightGap $spacing -topGap $spacing -bottomGap $spacing
deselectAll