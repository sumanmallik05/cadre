module bsg_manycore (
	clk_i, 
	reset_i, 
	hor_link_sif_i, 
	hor_link_sif_o, 
	ver_link_sif_i, 
	ver_link_sif_o);
   input clk_i;
   input reset_i;
   input [5517:0] hor_link_sif_i;
   output [5517:0] hor_link_sif_o;
   input [2847:0] ver_link_sif_i;
   output [2847:0] ver_link_sif_o;
endmodule
