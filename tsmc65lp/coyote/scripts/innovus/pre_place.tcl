################################################################################
#                          PRE-PLACE PLUG-IN
################################################################################
#
# This plug-in script is called before placeDesign from the run_place.tcl flow
# script.
#
################################################################################
# Example tasks include:
#          - Power planning related tasks which includes
#            - Power planning for power domains (ring/strap creations)
#            - Power Shut-off cell power hookup
####################################################################################


#setOptMode -fixFanoutLoad true
#setOptMode -setupTargetSlack 0.02


################################################################################
# POWER MESH CREATION
################################################################################

source $::env(INNOVUS_FLOW_DIR)/common/tcl/simplePowerPlanTsmc65lp.tcl


# Limit tiehi / tielo fanout
setTieHiLoMode -maxDistance 20

# clock gating properties
set_ccopt_property clone_clock_gates true
set_ccopt_property ccopt_merge_clock_gates true
set_ccopt_property merge_clock_gates true
