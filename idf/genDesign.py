#!/usr/bin/env python

import argparse  # argument parsing
import json  # json parsing
import sys  # exit function
import shutil  # File Manipulation
import os  # filesystem manipulation
import re  # Regular expressinso
from math import log, ceil


def fileSubst(pattern, replacement, filepath):
  # Function performs substitutions in a file given a pattern and replacement
  # ----------------------------------------------------------------------------
  # Read in the file
  with open(filepath, 'r') as f:
    filedata = f.read()

  # Replace the target string
  filedata = filedata.replace(pattern, replacement)

  # Write the file out again
  with open(filepath, 'w') as f:
    f.write(filedata)


def fileAppend(infile, outfile):
  # Function appends data in an input file to an output file
  # ----------------------------------------------------------------------------
  if not os.path.isfile(infile):
    print("Error: File specified in json config does not exist")
    print("Resolved File Path: " + infile)
    sys.exit(1)

  # Read in the file
  with open(infile, 'r') as f:
    filedata = f.read()

  # Write the file out
  with open(outfile, 'a') as f:
    f.write(filedata)


def toXCoord(x):
  # Converts idf coordinates to process/design coordinates
  # ----------------------------------------------------------------------------
  if unit_x == "cpp":
    return str(x * cpp)
  elif unit_y == "track_width":
    return str(x * bv_pitch)
  else:
    print("Error: Invalid unit_x")
    sys.exit(1)


def toYCoord(y):
  # Converts idf coordinates to process/design coordinates
  # ----------------------------------------------------------------------------
  if unit_y == "track_height":
    return str(y * bh_pitch)
  else:
    print("Error: Invalid unit_y")
    sys.exit(1)


def getSramMux(words):
  # Selects a valid parameter for mux based on words (use min mux)
  # ----------------------------------------------------------------------------
  if 16 < words < 512:  # Multiplexer 2 is valid for words between 16 and 512 in steps of 4
    return 2
  elif words < 1024:  # Multiplexer 4 is valid for words between 64 and 1024 in steps of 8
    return 4
  elif words < 2048:  # Multiplexer 8 is valid for words between 128 and 2048 in steps of 16
    return 8
  elif words < 4096:  # Multiplexer 16 is valid for words between 256 and 4096 in steps of 32
    return 16
  else:
    print("Error: Cannot determine sram mux value")
    sys.exit(1)


def toSide(s):
  # Converts idf "Sides" to process/design sides
  # ----------------------------------------------------------------------------
  return s.upper()


def toOrientation(val, platform):
  # Converts idf "orientation" to process/design orientation
  # ----------------------------------------------------------------------------
  orientationMap = {
      "N": "R0",
      "S": "R180",
      "E": "R270",
      "W": "R90",
      "FN": "MY",
      "FS": "MX",
      "FE": "MY90",
      "FW": "MX90"
  }
  if (platform == "tsmc16") and (val == "W" or val == "FW" or val == "E" or val == "FE"):
    print("Warning: Coercing orientation to R0")
    return "R0"
  else:
    return orientationMap[val]


def toLayer(layer_string):
  # SHAMELESSLY STOLEN FUNCTION FROM idf_conferter
  # ----------------------------------------------------------------------------
  match = re.match('metal-(B|BH|BV|T|TH|TV)([0-9]+)', layer_string)
  if match:
    rel = match.group(1)
    num = int(match.group(2))
    if rel == 'B':
      bot = 0
      offset = num - 1
      abs_num = bot + offset
    elif rel == 'BH':
      bot_h = (0 if first_metal_h else 1)
      offset = 2 * (num - 1)
      abs_num = bot_h + offset
    elif rel == 'BV':
      bot_v = (1 if first_metal_h else 0)
      offset = 2 * (num - 1)
      abs_num = bot_v + offset
    elif rel == 'T':
      top = len(layer_stack) - 1
      offset = num - 1
      abs_num = top - offset
    elif rel == 'TH':
      even = (len(layer_stack) % 2) == 0
      top_is_h = (even and not first_metal_h) or (not even and first_metal_h)
      top_h = (len(layer_stack) - 1 if top_is_h else len(layer_stack) - 2)
      offset = 2 * (num - 1)
      abs_num = top_h - offset
    elif rel == 'TV':
      even = (len(layer_stack) % 2) == 0
      top_is_h = (even and not first_metal_h) or (not even and first_metal_h)
      top_v = (len(layer_stack) - 2 if top_is_h else len(layer_stack) - 1)
      offset = 2 * (num - 1)
      abs_num = top_v - offset
    return str(layer_stack[abs_num])
  return None


def genTscm16Ram():
  os.makedirs(outputDir + "/src/main/memory")
  for sram in jsonCfg["harden"]["srams"]:
    awidth = int(ceil(log(sram["depth"], 2)))
    dwidth = sram["width"]
    # Generators require a minimum of 8bits
    if sram["width"] < 8:
      print("Warning: Coercing " + sram["name"] + " data width to 8")
      dwidth = 8
    if dwidth % 2:  # if not even
      dwidth += 1
      print("Warning: Coercing " +
            sram["name"] + " data width to " + str(dwidth))

    instname = sram_prefix + "_" + \
        sram_name_dict[sram["type"]] + "_" +\
        "lg" + str(awidth) + "_" +\
        "w" + str(dwidth) + "_" +\
        ("all" if sram["mask"] == 0 else "bit")

    f = open(outputDir + "/src/main/memory/" + instname +
             "." + sram_gen_dict[sram["type"]], 'w')
    f.write("instname=" + instname + "\n")
    f.write("words=" + str(sram["depth"]) + "\n")
    # Generators require a minimum of 8bits
    f.write("bits=" + str(dwidth) + "\n")
    f.write("mux=" + str(getSramMux(sram["depth"])) + "\n")
    f.write("write_mask=" + ("off" if sram["mask"] == 0 else "on") + "\n")
    f.write("corners=ffg_0p88v_0p88v_125c,ffgnp_0p88v_0p88v_0c,ffgnp_0p88v_0p88v_125c,ffgnp_0p88v_0p88v_m40c,ssgnp_0p72v_0p72v_0c,ssgnp_0p72v_0p72v_125c,ssgnp_0p72v_0p72v_m40c,tt_0p72v_0p72v_0c,tt_0p80v_0p80v_25c,tt_0p80v_0p80v_85c" + "\n")
    f.write("check_instname=off")
    f.close()


def genTscm65lpRam():
  os.makedirs(outputDir + "/src/main/memory")
  for sram in jsonCfg["harden"]["srams"]:
    awidth = int(ceil(log(sram["depth"], 2)))
    dwidth = sram["width"]
    if sram["width"] < 8:
      print("Warning: Coercing " + sram["name"] + " data width to 8")
      dwidth = 8
    if dwidth % 2:  # if not even
      dwidth += 1
      print("Warning: Coercing " +
            sram["name"] + " data width to " + str(dwidth))

    instname = sram_prefix + "_" + \
        sram_name_dict[sram["type"]] + "_" +\
        "lg" + str(awidth) + "_" +\
        "w" + str(dwidth) + "_" +\
        ("all" if sram["mask"] == 0 else "bit")

    f = open(outputDir + "/src/main/memory/" + instname +
             "." + sram_gen_dict[sram["type"]], 'w')
    f.write("instname=" + instname + "\n")
    f.write("words=" + str(sram["depth"]) + "\n")
    f.write("bits=" + str(dwidth) + "\n")
    f.write("mux=" + str(getSramMux(sram["depth"])) + "\n")
    f.write("write_mask=" + ("off" if sram["mask"] == 0 else "on") + "\n")
    f.write("corners=ff_1p32v_1p32v_m40c,ss_1p08v_1p08v_125c,tt_1p20v_1p20v_25c" + "\n")
    f.write("check_instname=off" + "\n")
    f.write("libname=" + instname)
    f.close()


# End Functions  ---------------------------------------------------------

# Parse and validate arguments
# ==============================================================================
parser = argparse.ArgumentParser(
    description='Generates cadre designs from idf specification')
parser.add_argument('--idfFile', '-i', required=True,
                    help='Path to the idf file describing the design')
parser.add_argument('--outputDir', '-o', required=True,
                    help='Output directory for generated design')
parser.add_argument('--constraint', '-c', required=True,
                    help='Name of the constraints to be used for the design')
parser.add_argument('--platform', '-p', required=True,
                    help='platform')
parser.add_argument('--verilog', '-v', required=True, nargs=argparse.REMAINDER,
                    help='Name of the Verilog file(s) for the design')
args = parser.parse_args()

if not os.path.isfile(args.idfFile):
  print("Error: idfFile does not exist")
  print("File Path: " + args.idfFile)
  sys.exit(1)


jsonCfgFilePath = args.idfFile
outputDir = args.outputDir
pickledVerilogs = args.verilog
pickledConstraints = args.constraint

# Script configuration
# ==============================================================================
skelDir = "./template/" + args.platform


if args.platform == "tsmc16":
  # The following are specific to tsmc16
  platform = "tsmc16"
  layer_stack = [1, 2, 3, 4, 5, 6, 7, 8, 9]
  first_metal_h = False
  fo4 = 10
  bv_pitch = 0.064  # M2 Pitch
  bh_pitch = 0.064  # M1 Pitch
  cpp = 0.096  # Unit tile x
  sram_prefix = "tsmc16"
  sram_name_dict = {
      "sram": "1rw",
      "rf": "1rf",
      "2sram": "1r1w",
      "2rf": "2rf"
  }
  sram_gen_dict = {
      "sram": "SRAM_SP_HDE",
      "rf": "RF_SP_HDE",
      "2sram": "SRAM_2P_UHDE",
      "2rf": "RF_2P_HSC"
  }

elif args.platform == "tsmc65lp":
  # The following are specific to tsmc65lp
  platform = "tsmc65lp"
  layer_stack = [1, 2, 3, 4, 5, 6, 7, 8, 9]
  first_metal_h = True
  fo4 = 10
  bv_pitch = 0.2  # M2 Pitch
  bh_pitch = 0.2  # M1 Pitch
  cpp = 0.2  # Unit tile x
  sram_prefix = "tsmc65lp"
  sram_name_dict = {
      "sram": "1rw",
      "rf": "1rf",
      "2sram": "1r1w",
      "2rf": "2rf"
  }
  sram_gen_dict = {
      "sram": "SRAM_SP_HDE",
      "rf": "RF_SP_HDF",
      "2sram": "SRAM_2P_UHDE",
      "2rf": "RF_2P_HDE"
  }


else:
  print("Error: Supported platforms are : tsmc16 tsmc65lp")
  sys.exit(1)

# Open and parse json config file
# ==============================================================================

try:
  refDir = os.path.dirname(jsonCfgFilePath)
  with open(jsonCfgFilePath) as f:
    jsonCfg = json.load(f)
except ValueError as e:
  print("Error occured opening or loading json file.")
  print >> sys.stderr, "Exception: %s" % str(e)
  sys.exit(1)


# TODO: Perform checks
#   - json version
#   - toplevel exists
#   - check that number of designs = number of floorplans (match the names)

# TODO: Print script configuration
toplevel = jsonCfg["toplevel"]
unit_x = jsonCfg["units"]["x"]
unit_y = jsonCfg["units"]["y"]


# Section: Setup a design
# ==============================================================================
# TODO: Make this a function that can be called for multiple designs

print("Creating cadre design for " + toplevel + " (" + outputDir + ")")

# Create directory/design
# ------------------------------------------------------------------------------
# TODO: if block design update output dir

# Copy skeleton to new directory
if os.path.isdir(outputDir):
  shutil.rmtree(outputDir)
shutil.copytree(skelDir, outputDir)

# Replace variables (design name and platform) in include.mk
fileSubst("@@@DESIGN_NAME@@@", jsonCfg["toplevel"], outputDir + "/include.mk")
fileSubst("@@@PLATFORM@@@", platform, outputDir + "/include.mk")
fileSubst("@@@FO4@@@", str(fo4), outputDir + "/include.mk")


# SYNTHESIS UPDATES
# ==============================================================================

# Update synthesis file list in dc.filelist.tcl
# ------------------------------------------------------------------------------
srcFileList = []

# for file in jsonCfg["synthesis"]["designs"][0]["src_files"]:
#   absFilePath = os.path.abspath(refDir + "/" + file)
#   if not os.path.isfile(absFilePath):
#     print("Error: File specified in json config does not exist")
#     print("Resolved File Path: " + absFilePath)
#     sys.exit(1)
#   srcFileList.append(absFilePath)

os.makedirs(outputDir + "/src/verilog")
for file in pickledVerilogs:
  shutil.copyfile(refDir + "/" + file, outputDir + "/src/verilog" + "/" + file)
  srcFileList.append("src/verilog" + "/" + file)


srcFileString = "set SVERILOG_SOURCE_FILES \"" + "\n".join(srcFileList) + "\""
fileSubst("@@@SVERILOG_SOURCE_FILES@@@", srcFileString,
          outputDir + "/scripts/dc/dc.filelist.tcl")

# update include_dirs
# ------------------------------------------------------------------------------
# TODO

# Update verilog parameters list in dc.read_design.tcl
# ------------------------------------------------------------------------------
# parameterList = []
# for param in jsonCfg["synthesis"]["designs"][0]["parameters"]:
#   parameterList.append(param + "=" +str(jsonCfg["synthesis"]["designs"][0]["parameters"][param]))
# parameterString = "set PARAMETERS \"" + ",".join(parameterList) + "\""

parameterString = "set PARAMETERS \"\""
fileSubst("@@@VERILOG_PARAMETERS@@@", parameterString,
          outputDir + "/scripts/dc/dc.read_design.tcl")

# Update design constraints in constraints.tcl
# ------------------------------------------------------------------------------
# for file in jsonCfg["synthesis"]["designs"][0]["constraint_files"]:
#   fileAppend( os.path.abspath(refDir + "/" + file), outputDir + "/scripts/dc/constraints.tcl")
fileAppend(os.path.abspath(refDir + "/" + pickledConstraints),
           outputDir + "/scripts/dc/constraints.tcl")


# update dont_touch list in dc.read_design.tcl
# ------------------------------------------------------------------------------
# TODO

# create memory spec
# ------------------------------------------------------------------------------
# for file in jsonCfg["synthesis"]["designs"][0]["memory"]:


# FLOORPLANNING
# ==============================================================================

# Handle floorplan
# ------------------------------------------------------------------------------
core_width = jsonCfg["area"]["core"]["urx"] - jsonCfg["area"]["core"]["llx"]
core_height = jsonCfg["area"]["core"]["ury"] - jsonCfg["area"]["core"]["lly"]
core_margin_t = jsonCfg["area"]["die"]["ury"] - jsonCfg["area"]["core"]["ury"]
core_margin_b = jsonCfg["area"]["die"]["lly"] - jsonCfg["area"]["core"]["lly"]
core_margin_r = jsonCfg["area"]["die"]["urx"] - jsonCfg["area"]["core"]["urx"]
core_margin_l = jsonCfg["area"]["die"]["llx"] - jsonCfg["area"]["core"]["llx"]

fileSubst("@@@core_width@@@",    toXCoord(core_width),
          outputDir + "/scripts/innovus/always_source.tcl")
fileSubst("@@@core_height@@@",   toYCoord(core_height),
          outputDir + "/scripts/innovus/always_source.tcl")
fileSubst("@@@core_margin_t@@@", toYCoord(abs(core_margin_t)),
          outputDir + "/scripts/innovus/always_source.tcl")
fileSubst("@@@core_margin_b@@@", toYCoord(abs(core_margin_b)),
          outputDir + "/scripts/innovus/always_source.tcl")
fileSubst("@@@core_margin_r@@@", toXCoord(abs(core_margin_r)),
          outputDir + "/scripts/innovus/always_source.tcl")
fileSubst("@@@core_margin_l@@@", toXCoord(abs(core_margin_l)),
          outputDir + "/scripts/innovus/always_source.tcl")

# place macros
# ------------------------------------------------------------------------------
macro_placement = ""
for macro in jsonCfg["designs"][0]["place"]:
  macro_placement += 'placeInstance ' +  macro["name"] +  \
      ' ' + toXCoord(macro["x"]) + \
      ' ' + toYCoord(macro["y"]) + \
      ' ' + toOrientation(macro["orientation"], platform) + '\n'

fileSubst("@@@MACRO_PLACEMENT@@@", macro_placement,
          outputDir + "/scripts/innovus/floorplan.tcl")
# generate sram wrappers
# ------------------------------------------------------------------------------
# TODO, currently manual


# generate sram specs
# ------------------------------------------------------------------------------

if platform == "tsmc16":
  genTscm16Ram()
elif platform == "tsmc65lp":
  genTscm65lpRam()
else:
  sys.exit(1)


# place pins
# ------------------------------------------------------------------------------
io_placement = ""
for io in jsonCfg["designs"][0]["io_ports"]:
  io_placement += 'editPin -pin ' +  io["name"] +  \
      ' -fixedPin 1' \
      ' -layer ' +  toLayer(io["layer"]) +  \
      ' -snap TRACK' +  \
      ' -side ' +  toSide(io["side"]) +  \
      ' -assign {' + toXCoord(io["x"]) + \
      ' ' + toYCoord(io["y"]) + '} \n'

fileSubst("@@@IO_PIN_PLACEMENT@@@", io_placement,
          outputDir + "/scripts/innovus/floorplan.tcl")

# placement bounds
# ------------------------------------------------------------------------------
placement_bounds = ""
for bound in jsonCfg["designs"][0]["placement_bounds"]:
  if bound["type"] == "hard":
    placement_bounds += "createFence "
  elif bound["type"] == "soft":
    placement_bounds += "createGuide "
  else:
    print("Error: placement bound type not supported")
    sys.exit(1)

  placement_bounds += bound["inst"] + " " + toXCoord(bound["llx"]) +  " " + \
      toYCoord(bound["lly"]) + " " + \
      toXCoord(bound["urx"]) + " " + \
      toYCoord(bound["ury"]) + "\n"


fileSubst("@@@PLACEMENT_GUIDE@@@", placement_bounds,
          outputDir + "/scripts/innovus/floorplan.tcl")

# power plan
# ------------------------------------------------------------------------------
# Finish section to setup a design.

print("Generation complete for " + toplevel + "\n\n")
