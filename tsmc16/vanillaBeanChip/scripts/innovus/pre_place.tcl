setOptMode -fixFanoutLoad true

################################################################################
#                          PRE-PLACE PLUG-IN
################################################################################
#
# This plug-in script is called before placeDesign from the run_place.tcl flow
# script.
#
################################################################################
# Example tasks include:
#          - Power planning related tasks which includes
#            - Power planning for power domains (ring/strap creations)
#            - Power Shut-off cell power hookup
################################################################################

source $::env(INNOVUS_FLOW_DIR)/common/tcl/addIoFiller.tcl


# Define cell sizes
set mem_x [dbGet [dbGetCellByName tsmc16_1rw_lg10_w32_all].size_x]
set mem_y [dbGet [dbGetCellByName tsmc16_1rw_lg10_w32_all].size_y]

set rf_x  [dbGet [dbGetCellByName tsmc16_2rf_lg5_w32_all].size_x]
set rf_y  [dbGet [dbGetCellByName tsmc16_2rf_lg5_w32_all].size_y]

################################################################################
# POWER CONFIGURATION PARAMETERS
################################################################################
# Lower grid has already been done by PGA


################################################################################
# POWER MESH CREATION
################################################################################

if {([string tolower $vars(use_pga)] != "true")} {
    set coreBox_llx [dbGet top.fplan.coreBox_llx]
    set coreBox_lly [dbGet top.fplan.coreBox_lly]
    set coreBox_urx [dbGet top.fplan.coreBox_urx]
    set coreBox_ury [dbGet top.fplan.coreBox_ury]


    source $::env(INNOVUS_FLOW_DIR)/common/tcl/simplePowerPlan.tcl
}


################################################################################
# WIRBOND
################################################################################

setFlipChipMode -connectPowerCellToBump true \
                -honor_bump_connect_target_constraint true \
                -ignore_pad_type_check true

fcroute -type signal \
        -designStyle pio \
        -layerChangeBotLayer AP \
        -layerChangeTopLayer AP

if {$vars(debug_power)} {
  saveDesign $vars(dbs_dir)/fcroute.enc -compress
}

# suspend
# deselectAll
# editSelect -net {VDD VSS} -layer {M1 M2 M3 M4 M5 M6 M7 M8 M9}
# editDelete -selected

