###################################################################

# Created by write_sdc on Wed Jun 12 14:00:03 2019

###################################################################
set sdc_version 2.0

set_units -time ns -resistance MOhm -capacitance fF -voltage V -current mA
create_clock [get_ports clk]  -name core_clock  -period 10  -waveform {0 5}
