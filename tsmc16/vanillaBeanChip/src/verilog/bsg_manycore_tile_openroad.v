module bsg_manycore_tile_openroad (
  inout wire [15:0] PAD_N,
  inout wire [15:0] PAD_E,
  inout wire [15:0] PAD_W,
  inout wire [15:0] PAD_S
);

  wire [15:0] PBIDIRN_N_A ;
  wire [15:0] PBIDIRN_N_OE;
  wire [15:0] PBIDIRN_N_IE;
  wire [15:0] PBIDIRN_N_Y ;

  wire [15:0] PBIDIRN_E_A ;
  wire [15:0] PBIDIRN_E_OE;
  wire [15:0] PBIDIRN_E_IE;
  wire [15:0] PBIDIRN_E_Y ;

  wire [15:0] PBIDIRN_S_A ;
  wire [15:0] PBIDIRN_S_OE;
  wire [15:0] PBIDIRN_S_IE;
  wire [15:0] PBIDIRN_S_Y ;

  wire [15:0] PBIDIRN_W_A ;
  wire [15:0] PBIDIRN_W_OE;
  wire [15:0] PBIDIRN_W_IE;
  wire [15:0] PBIDIRN_W_Y ;


  PBIDIRN_18_18_FS_DR_V PBIDIRN_N[15:0] ( .A(PBIDIRN_N_A), .DS0(1'b0), .DS1(1'b0), .IE(PBIDIRN_N_IE),
    .IS(1'b0), .OE(PBIDIRN_N_OE), .PE(1'b0), .POE(1'b0), .PS(1'b0), .RTO(1'b1),
    .SNS(1'b1), .SR(1'b0), .PAD(PAD_N), .Y(PBIDIRN_N_Y) );

  PBIDIRN_18_18_FS_DR_H PBIDIRN_E[15:0] ( .A(PBIDIRN_E_A), .DS0(1'b0), .DS1(1'b0), .IE(PBIDIRN_E_IE),
    .IS(1'b0), .OE(PBIDIRN_E_OE), .PE(1'b0), .POE(1'b0), .PS(1'b0), .RTO(1'b1),
    .SNS(1'b1), .SR(1'b0), .PAD(PAD_E), .Y(PBIDIRN_E_Y) );

  PBIDIRN_18_18_FS_DR_H PBIDIRN_W[15:0] ( .A(PBIDIRN_W_A), .DS0(1'b0), .DS1(1'b0), .IE(PBIDIRN_W_IE),
    .IS(1'b0), .OE(PBIDIRN_W_OE), .PE(1'b0), .POE(1'b0), .PS(1'b0), .RTO(1'b1),
    .SNS(1'b1), .SR(1'b0), .PAD(PAD_W), .Y(PBIDIRN_W_Y) );

  PBIDIRN_18_18_FS_DR_V PBIDIRN_S[15:0] ( .A(PBIDIRN_S_A), .DS0(1'b0), .DS1(1'b0), .IE(PBIDIRN_S_IE),
    .IS(1'b0), .OE(PBIDIRN_S_OE), .PE(1'b0), .POE(1'b0), .PS(1'b0), .RTO(1'b1),
    .SNS(1'b1), .SR(1'b0), .PAD(PAD_S), .Y(PBIDIRN_S_Y) );


  // east and west is input
  assign PBIDIRN_E_OE = {15{1'b0}};
  assign PBIDIRN_E_IE = {15{1'b1}};
  assign PBIDIRN_W_OE = {15{1'b0}};
  assign PBIDIRN_W_IE = {15{1'b1}};

  // south and north is output
  assign PBIDIRN_S_OE = {15{1'b1}};
  assign PBIDIRN_S_IE = {15{1'b0}};
  assign PBIDIRN_N_OE = {15{1'b1}};
  assign PBIDIRN_N_IE = {15{1'b0}};


  // gcd i_gcd (
  //   .clk     (PBIDIRN_N_Y[0]            ), // input  wire [   0:0]
  //   .req_msg ({PBIDIRN_E_Y, PBIDIRN_W_Y}), // input  wire [  31:0]
  //   .req_rdy (PBIDIRN_N_A[1]            ), // output wire [   0:0]
  //   .req_val (PBIDIRN_N_Y[2]            ), // input  wire [   0:0]
  //   .reset   (PBIDIRN_N_Y[3]            ), // input  wire [   0:0]
  //   .resp_msg(PBIDIRN_S_A               ), // output wire [  15:0]
  //   .resp_rdy(PBIDIRN_N_Y[4]            ), // input  wire [   0:0]
  //   .resp_val(PBIDIRN_N_A[5]            )  // output wire [   0:0]
  // );

  wire            clk_i       ;
  wire            reset_i     ;
  wire [     3:0] my_x_i      ;
  wire [     4:0] my_y_i      ;
  wire [4*89-1:0] link_in_int ;
  wire [4*89-1:0] link_out_int;

  assign clk_i   = PBIDIRN_E_Y[0];
  assign reset_i = PBIDIRN_E_Y[1];
  assign my_x_i  = {4{PBIDIRN_E_Y[2]}};
  assign my_y_i  = {5{PBIDIRN_E_Y[3]}};

  assign link_in_int = {
    PBIDIRN_W_Y[15:0], PBIDIRN_E_Y[15:4],
    PBIDIRN_W_Y[15:0], PBIDIRN_E_Y[15:4],
    PBIDIRN_W_Y[15:0], PBIDIRN_E_Y[15:4],
    PBIDIRN_W_Y[15:0], PBIDIRN_E_Y[15:4],
    PBIDIRN_W_Y[15:0], PBIDIRN_E_Y[15:4],
    PBIDIRN_W_Y[15:0], PBIDIRN_E_Y[15:4],
    PBIDIRN_W_Y[15:0], PBIDIRN_E_Y[15:4],
    PBIDIRN_W_Y[15:0], PBIDIRN_E_Y[15:4],
    PBIDIRN_W_Y[15:0], PBIDIRN_E_Y[15:4],
    PBIDIRN_W_Y[15:0], PBIDIRN_E_Y[15:4],
    PBIDIRN_W_Y[15:0], PBIDIRN_E_Y[15:4],
    PBIDIRN_W_Y[15:0], PBIDIRN_E_Y[15:4],
    PBIDIRN_W_Y[15:0], PBIDIRN_E_Y[3:0]};


  assign PBIDIRN_S_A[0]  = ^link_out_int[25:0];
  assign PBIDIRN_S_A[1]  = ^link_out_int[50:25];
  assign PBIDIRN_S_A[2]  = ^link_out_int[75:50];
  assign PBIDIRN_S_A[3]  = ^link_out_int[100:75];
  assign PBIDIRN_S_A[4]  = ^link_out_int[125:100];
  assign PBIDIRN_S_A[5]  = ^link_out_int[150:125];
  assign PBIDIRN_S_A[6]  = ^link_out_int[175:150];
  assign PBIDIRN_S_A[7]  = ^link_out_int[200:175];
  assign PBIDIRN_S_A[8]  = ^link_out_int[225:200];
  assign PBIDIRN_S_A[9]  = ^link_out_int[250:225];
  assign PBIDIRN_S_A[10] = ^link_out_int[275:250];
  assign PBIDIRN_S_A[11] = ^link_out_int[300:275];
  assign PBIDIRN_S_A[12] = ^link_out_int[325:300];
  assign PBIDIRN_S_A[13] = ^link_out_int[350:325];
  assign PBIDIRN_S_A[14] = ^link_out_int[351:350];



  bsg_manycore_tile #(
    .bank_size_p   (1024   ),
    .num_banks_p   (1      ),
    .imem_size_p   (1024   ),
    .x_cord_width_p(4      ),
    .y_cord_width_p(5      ),
    .data_width_p  (32     ),
    .addr_width_p  (20     ),
    .stub_p        (4'b0000)
  ) i_bsg_manycore_tile (
    .clk_i   (clk_i       ),
    .reset_i (reset_i     ),
    .link_in (link_in_int ),
    .link_out(link_out_int),
    .my_x_i  (my_x_i      ),
    .my_y_i  (my_y_i      )
  );

endmodule