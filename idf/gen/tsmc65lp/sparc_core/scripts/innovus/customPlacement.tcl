set margin 3 ;# Set specifically to avoid dangling stripes in the margin
set spacing 5
set bigSpace 20

# Bottom
placeInstance sparc0/lsu/lsu/dcache/dcache_way_0/mem \
  [expr [dbGet top.fPlan.coreBox_sizex]/2 -  \
    [expr [dbGet [dbGet -p1 top.insts.name sparc0/lsu/lsu/dcache/dcache_way_0/mem].box_sizey] + \
          $spacing + \
          [dbGet [dbGet -p1 top.insts.name sparc0/lsu/lsu/dcache/dcache_way_1/mem].box_sizey] + \
          $spacing + \
          [dbGet [dbGet -p1 top.insts.name sparc0/lsu/lsu/dcache/dcache_way_2/mem].box_sizey] + \
          $spacing + \
          [dbGet [dbGet -p1 top.insts.name sparc0/lsu/lsu/dcache/dcache_way_3/mem].box_sizey] \
    ]/2\
  ] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90
placeInstance sparc0/lsu/lsu/dcache/dcache_way_1/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name sparc0/lsu/lsu/dcache/dcache_way_0/mem].box_urx]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90
placeInstance sparc0/lsu/lsu/dcache/dcache_way_2/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name sparc0/lsu/lsu/dcache/dcache_way_1/mem].box_urx]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90
placeInstance sparc0/lsu/lsu/dcache/dcache_way_3/mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name sparc0/lsu/lsu/dcache/dcache_way_2/mem].box_urx]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90

placeInstance sparc0/lsu/lsu/dtag/cache/sram_l1d_tag/genblk1_mem \
  [expr $bigSpace*5 + [dbGet [dbGet -p1 top.insts.name sparc0/lsu/lsu/dcache/dcache_way_3/mem].box_urx]] \
  [expr $margin + [dbGet top.fplan.coreBox_lly]] \
  R90

# Top

placeInstance sparc0/ifu/ifu/icd/icache_way_0/sram_l1i_data/genblk1_mem \
  [expr [dbGet top.fPlan.coreBox_sizex]/2 -  \
    [expr [dbGet [dbGet -p1 top.insts.name sparc0/ifu/ifu/icd/icache_way_0/sram_l1i_data/genblk1_mem].box_sizey] + \
          $spacing + \
          [dbGet [dbGet -p1 top.insts.name sparc0/ifu/ifu/icd/icache_way_1/sram_l1i_data/genblk1_mem].box_sizey] + \
          $spacing + \
          [dbGet [dbGet -p1 top.insts.name sparc0/ifu/ifu/icd/icache_way_2/sram_l1i_data/genblk1_mem].box_sizey] + \
          $spacing + \
          [dbGet [dbGet -p1 top.insts.name sparc0/ifu/ifu/icd/icache_way_3/sram_l1i_data/genblk1_mem].box_sizey] \
    ]/2 \
  ] \
 [expr [dbGet top.fPlan.coreBox_ury] - $margin - [dbGet [dbGet -p1 top.insts.name sparc0/ifu/ifu/icd/icache_way_0/sram_l1i_data/genblk1_mem].box_sizex]] \
  R90

placeInstance sparc0/ifu/ifu/icd/icache_way_1/sram_l1i_data/genblk1_mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name sparc0/ifu/ifu/icd/icache_way_0/sram_l1i_data/genblk1_mem].box_urx]] \
  [expr [dbGet top.fPlan.coreBox_ury] - $margin - [dbGet [dbGet -p1 top.insts.name sparc0/ifu/ifu/icd/icache_way_1/sram_l1i_data/genblk1_mem].box_sizex]] \
  R90
placeInstance sparc0/ifu/ifu/icd/icache_way_2/sram_l1i_data/genblk1_mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name sparc0/ifu/ifu/icd/icache_way_1/sram_l1i_data/genblk1_mem].box_urx]] \
  [expr [dbGet top.fPlan.coreBox_ury] - $margin - [dbGet [dbGet -p1 top.insts.name sparc0/ifu/ifu/icd/icache_way_2/sram_l1i_data/genblk1_mem].box_sizex]] \
  R90
placeInstance sparc0/ifu/ifu/icd/icache_way_3/sram_l1i_data/genblk1_mem \
  [expr $spacing + [dbGet [dbGet -p1 top.insts.name sparc0/ifu/ifu/icd/icache_way_2/sram_l1i_data/genblk1_mem].box_urx]] \
  [expr [dbGet top.fPlan.coreBox_ury] - $margin - [dbGet [dbGet -p1 top.insts.name sparc0/ifu/ifu/icd/icache_way_3/sram_l1i_data/genblk1_mem].box_sizex]] \
  R90

placeInstance sparc0/ifu/ifu/ict/cache/sram_l1i_tag/genblk1_mem \
  [expr $bigSpace*5 + [dbGet [dbGet -p1 top.insts.name sparc0/ifu/ifu/icd/icache_way_3/sram_l1i_data/genblk1_mem].box_urx]] \
  [expr [dbGet top.fPlan.coreBox_ury] - $margin - [dbGet [dbGet -p1 top.insts.name sparc0/ifu/ifu/ict/cache/sram_l1i_tag/genblk1_mem].box_sizex]] \
  R90


placeInstance sparc0/ffu/ffu/frf/regfile/sram_1rw_128x78/genblk1_mem \
  [expr [dbGet top.fPlan.coreBox_sizex]/2]  \
  [expr [dbGet top.fPlan.coreBox_sizey]/2]  \
  R0


selectInst [join "[dbGet -e [dbGet -p2 top.insts.cell.name $::env(PLATFORM)_*].name]"]
cutRow -selected -leftGap  $spacing -rightGap $spacing -topGap $spacing -bottomGap $spacing
deselectAll