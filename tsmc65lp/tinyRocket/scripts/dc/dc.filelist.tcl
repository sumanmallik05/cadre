# File order matters
set SVERILOG_SOURCE_FILES "
$::env(DESIGN_ROOT_DIR)/src/verilog/AsyncResetReg.v
$::env(DESIGN_ROOT_DIR)/src/verilog/ClockDivider2.v
$::env(DESIGN_ROOT_DIR)/src/verilog/ClockDivider3.v
$::env(DESIGN_ROOT_DIR)/src/verilog/plusarg_reader.v
$::env(DESIGN_ROOT_DIR)/src/verilog/freechips.rocketchip.system.TinyConfig.hard_srams.v
$::env(DESIGN_ROOT_DIR)/src/verilog/freechips.rocketchip.system.TinyConfig.v
"


set NETLIST_SOURCE_FILES ""

