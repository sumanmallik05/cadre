module tsmc16_2rf_lg5_w32_all (QA, CLKA, CENA, AA, CLKB, CENB, AB, DB, STOV, EMAA,
    EMASA, EMAB, RET1N);

  parameter ASSERT_PREFIX = "";
  parameter BITS = 32;
  parameter WORDS = 32;
  parameter MUX = 1;
  parameter MEM_WIDTH = 32; // redun block size 1, 16 on left, 16 on right
  parameter MEM_HEIGHT = 32;
  parameter WP_SIZE = 32 ;
  parameter UPM_WIDTH = 3;
  parameter UPMW_WIDTH = 0;
  parameter UPMS_WIDTH = 1;
  parameter ROWS = 32;

  output [31:0] QA;
  input  CLKA;
  input  CENA;
  input [4:0] AA;
  input  CLKB;
  input  CENB;
  input [4:0] AB;
  input [31:0] DB;
  input  STOV;
  input [2:0] EMAA;
  input  EMASA;
  input [2:0] EMAB;
  input  RET1N;

endmodule
