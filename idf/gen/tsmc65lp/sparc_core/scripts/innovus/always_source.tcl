####################################################################################
#                              ALWAYS_SOURCE PLUG-IN
#####################################################################################
#
# This plug-in script is called from all flow scripts after loading the setup.tcl
# but after to loading the design data.  It can be used to set variables that affect
# non-persistent information
#
#####################################################################################
set t_pitch [dbGet top.fPlan.coreSite.size_y]     ;# Pitch between power rails (standard cell height)
set f_pitch [dbGet head.finGridPitch]             ;# Pitch between fins

# Variables used by Power Planning scripts
set pwr_net_list {VDD VSS}              ;# List of Power nets

set p_rng_w      4.0                  ;# Power ring metal width
set p_rng_s      2.0                  ;# Power ring metal space

set p_str_w      3.0                  ;# Power stripe metal width
set p_str_s      37.8                  ;# Power stripe metal space
set p_str_p      [expr 2*$p_str_w + 2*$p_str_s]                  ;# Power stripe metal space


# Floorplan Variables
set core_margin_t 0.0
set core_margin_b 0.0
set core_margin_r 0.0
set core_margin_l 0.0

# set core_width   1080.0 ;# Core Area Width
# set core_height  2100.0 ;# Core Area Height

set core_width   1480.0 ;# Core Area Width
set core_height  2600.0 ;# Core Area Height

