module SyncSpRamBeNx64 #(
  parameter ADDR_WIDTH = 10  ,
  parameter DATA_DEPTH = 1024, // usually 2**ADDR_WIDTH, but can be lower
  parameter OUT_REGS   = 0   , // set to 1 to enable outregs
  parameter SIM_INIT   = 0     // will not be synthesized
                               // 0: no init, 1: zero init, 2: random init, 3: deadbeef init
                               // note: on verilator, 2 is not supported. define the VERILATOR macro to work around.
) (
  input  logic                  Clk_CI   ,
  input  logic                  Rst_RBI  ,
  input  logic                  CSel_SI  ,
  input  logic                  WrEn_SI  ,
  input  logic [           7:0] BEn_SI   ,
  input  logic [          63:0] WrData_DI,
  input  logic [ADDR_WIDTH-1:0] Addr_DI  ,
  output logic [          63:0] RdData_DO
);

// WARNING: This module (SyncSpRamBeNx64) is only expected to be instantiated
// with the following parameters in this design

// SyncSpRamBeNx64_00000008_00000100_0_2

  if (ADDR_WIDTH == 16'h8 && DATA_DEPTH == 16'h100)
    begin: macro

      wire [63:0] wen = {  {8{BEn_SI[7]}}
                          ,{8{BEn_SI[6]}}
                          ,{8{BEn_SI[5]}}
                          ,{8{BEn_SI[4]}}
                          ,{8{BEn_SI[3]}}
                          ,{8{BEn_SI[2]}}
                          ,{8{BEn_SI[1]}}
                          ,{8{BEn_SI[0]}}};

    tsmc16_1rw_lg8_w64_byte mem (
      .CLK  (Clk_CI   ),
      .Q    (RdData_DO), // out
      .CEN  (~CSel_SI  ), // lo true
      .WEN  (wen      ),
      .GWEN (~WrEn_SI  ), // lo true
      .A    (Addr_DI  ), // in
      .D    (WrData_DI), // in
      .STOV (1'd0     ), // Self-timing Override - disabled
      .EMA  (3'd3     ), // Extra Margin Adjustment - default value
      .EMAW (2'd1     ), // Extra Margin Adjustment Write - default value
      .EMAS (1'd0     ), // Extra Margin Adjustment Sense Amp. - default value
      .RET1N(1'b1     )  // Retention Mode (active low) - disabled
    );

    end // block: macro


endmodule // SyncSpRamBeNx64
