#DESIGNS += $(wildcard asap7/*)
#DESIGNS += $(wildcard freepdk45/*)
#DESIGNS += $(wildcard gf14/*)
#DESIGNS += $(wildcard gpdk45/*)
#DESIGNS += $(wildcard tsmc16/*)
#DESIGNS += $(wildcard tsmc45/*)
DESIGNS += $(wildcard tsmc65lp/*)
DESIGNS += $(wildcard idf/gen/tsmc65lp/*)


all: $(foreach design,$(DESIGNS),test-$(design))
# .PHONY: $(DESIGNS)

$(foreach design,$(DESIGNS),test-$(design)): test-% :
	$(MAKE) -C $*

# For SDSC
# $(foreach design,$(DESIGNS),test-$(design)): test-% :
# 	echo cd $(CURDIR)/$* > $*/make.sh
# 	echo export INNOVUS_OPTIONS=\"-no_gui -64\" >> $*/make.sh
# 	echo make >> $*/make.sh
# 	chmod +x $*/make.sh
# 	mkdir -p $*/vpath
# 	touch $*/vpath/check_tools
# 	qsub -q home -l walltime=12:00:00 $*/make.sh

bleach: $(foreach design,$(DESIGNS),bleach-$(design))

$(foreach design,$(DESIGNS),bleach-$(design)): bleach-% :
	$(MAKE) -C $* bleach_all

list:
	@echo $(DESIGNS)

