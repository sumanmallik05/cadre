# Set simulation parameters specific to this design
#
# Very few things should be set here, since they should be specific to the
# design (e.g., clock periods).

#VCS_POSTAPR_SDF_OPTIONS += +define+CORE_0_PERIOD=10000
#VCS_POSTAPR_SDF_OPTIONS += +define+CORE_1_PERIOD=10000
#VCS_POSTAPR_SDF_OPTIONS += +define+IO_MASTER_0_PERIOD=9500
#VCS_POSTAPR_SDF_OPTIONS += +define+IO_MASTER_1_PERIOD=9500

# IMPORTANT: Rocket bsg_designs use different defines for the clocks

VCS_POSTAPR_SDF_OPTIONS += +define+CORE_CLOCK_PERIOD_PS=12500
VCS_POSTAPR_SDF_OPTIONS += +define+IO_CLOCK_PERIOD_PS=20000
VCS_POSTAPR_SDF_OPTIONS += +define+MANYCORE_CLOCK_PERIOD_PS=15000

# Performance optimization

#VCS_POSTAPR_SDF_OPTIONS += +timopt+15000ps

# SDF annotation
#
# Annotate each block with their SDF file

VCS_POSTAPR_SDF_SCOPE = test_five.dut.chip

# BNN SDF annotation

VCS_POSTAPR_SDF_bnn_scope     = test_five.dut.chip.g.acc.bnn_rocc_inst
VCS_POSTAPR_SDF_bnn_sdf       = $(DESIGN_ROOT_DIR)/blocks/bnn/export/bsg_rocket_accelerator_bnn.sdf
VCS_POSTAPR_SDF_OPTIONS      += -sdf max:$(VCS_POSTAPR_SDF_bnn_scope):$(VCS_POSTAPR_SDF_bnn_sdf)

SDF_FILES += $(VCS_POSTAPR_SDF_bnn_sdf)

# Coyote SDF annotation

VCS_POSTAPR_SDF_coyote0_scope = test_five.dut.chip.g.n_0__clnt
VCS_POSTAPR_SDF_coyote1_scope = test_five.dut.chip.g.n_1__clnt
VCS_POSTAPR_SDF_coyote2_scope = test_five.dut.chip.g.n_2__clnt
VCS_POSTAPR_SDF_coyote3_scope = test_five.dut.chip.g.n_3__clnt
VCS_POSTAPR_SDF_coyote4_scope = test_five.dut.chip.g.n_4__clnt

VCS_POSTAPR_SDF_coyote_sdf    = $(DESIGN_ROOT_DIR)/blocks/coyote_node/export/bsg_rocket_node_client_rocc.sdf

VCS_POSTAPR_SDF_OPTIONS      += -sdf max:$(VCS_POSTAPR_SDF_coyote0_scope):$(VCS_POSTAPR_SDF_coyote_sdf)
VCS_POSTAPR_SDF_OPTIONS      += -sdf max:$(VCS_POSTAPR_SDF_coyote1_scope):$(VCS_POSTAPR_SDF_coyote_sdf)
VCS_POSTAPR_SDF_OPTIONS      += -sdf max:$(VCS_POSTAPR_SDF_coyote2_scope):$(VCS_POSTAPR_SDF_coyote_sdf)
VCS_POSTAPR_SDF_OPTIONS      += -sdf max:$(VCS_POSTAPR_SDF_coyote3_scope):$(VCS_POSTAPR_SDF_coyote_sdf)
VCS_POSTAPR_SDF_OPTIONS      += -sdf max:$(VCS_POSTAPR_SDF_coyote4_scope):$(VCS_POSTAPR_SDF_coyote_sdf)

SDF_FILES += $(VCS_POSTAPR_SDF_coyote_sdf)

# Manycore SDF annotation

VCS_POSTAPR_SDF_manycore_scope = test_five.dut.chip.g.acc.manycore_rocc_inst.UUT
VCS_POSTAPR_SDF_manycore_sdf   = $(DESIGN_ROOT_DIR)/blocks/manycore_hierarchical/export/bsg_manycore.sdf
VCS_POSTAPR_SDF_OPTIONS       += -sdf max:$(VCS_POSTAPR_SDF_manycore_scope):$(VCS_POSTAPR_SDF_manycore_sdf)

SDF_FILES += $(VCS_POSTAPR_SDF_manycore_sdf)

# DC-DC Manycore SDF annotation

VCS_POSTAPR_SDF_dcdcmanycore_scope = test_five.dut.chip.g.dcdc_block.dcdc_manycore
VCS_POSTAPR_SDF_dcdcmanycore_sdf   = $(DESIGN_ROOT_DIR)/blocks/dcdc_manycore/export/dcdc_manycore.sdf
VCS_POSTAPR_SDF_OPTIONS           += -sdf max:$(VCS_POSTAPR_SDF_dcdcmanycore_scope):$(VCS_POSTAPR_SDF_dcdcmanycore_sdf)

SDF_FILES += $(VCS_POSTAPR_SDF_dcdcmanycore_sdf)

# Tile SDF annotation

VCS_POSTAPR_SDF_tile_sdf = $(DESIGN_ROOT_DIR)/blocks/manycore_hierarchical/export/bsg_manycore_tile.sdf

manycore_x_dim = $(shell seq 0 15)
manycore_y_dim = $(shell seq 0 30)

define tile_sdf_template
VCS_POSTAPR_SDF_OPTIONS += -sdf max:test_five.dut.chip.g.acc.manycore_rocc_inst.UUT.y_$(2)__x_$(1)__tile:$(VCS_POSTAPR_SDF_tile_sdf)
endef

$(foreach x,$(manycore_x_dim), \
  $(foreach y,$(manycore_y_dim), \
    $(eval $(call tile_sdf_template,$x,$y))))

SDF_FILES += $(VCS_POSTAPR_SDF_tile_sdf)

# DC-DC Tile SDF annotation

VCS_POSTAPR_SDF_dcdctile_sdf = $(DESIGN_ROOT_DIR)/blocks/dcdc_manycore/export/bsg_manycore_tile_1024_1_1024_1_3_32_20_0_00000000_00000000_0.sdf

dcdcmanycore_x_dim = $(shell seq 0 1)
dcdcmanycore_y_dim = $(shell seq 0 4)

define dcdctile_sdf_template
VCS_POSTAPR_SDF_OPTIONS += -sdf max:test_five.dut.chip.g.dcdc_block.dcdc_manycore.y_$(2)__x_$(1)__tile:$(VCS_POSTAPR_SDF_dcdctile_sdf)
endef

$(foreach x,$(dcdcmanycore_x_dim), \
  $(foreach y,$(dcdcmanycore_y_dim), \
    $(eval $(call dcdctile_sdf_template,$x,$y))))

SDF_FILES += $(VCS_POSTAPR_SDF_dcdctile_sdf)

# PLL SDF annotation

VCS_POSTAPR_SDF_pll_sdf   = $(DESIGN_ROOT_DIR)/blocks/pll/export/pll.sdf

VCS_POSTAPR_SDF_corepll_scope = test_five.dut.chip.core_clk_pll
VCS_POSTAPR_SDF_iopll_scope = test_five.dut.chip.io_master_clk_pll
VCS_POSTAPR_SDF_manycorepll_scope = test_five.dut.chip.manycore_clk_pll

VCS_POSTAPR_SDF_OPTIONS  += -sdf max:$(VCS_POSTAPR_SDF_corepll_scope):$(VCS_POSTAPR_SDF_pll_sdf)
VCS_POSTAPR_SDF_OPTIONS  += -sdf max:$(VCS_POSTAPR_SDF_iopll_scope):$(VCS_POSTAPR_SDF_pll_sdf)
VCS_POSTAPR_SDF_OPTIONS  += -sdf max:$(VCS_POSTAPR_SDF_manycorepll_scope):$(VCS_POSTAPR_SDF_pll_sdf)

SDF_FILES += $(VCS_POSTAPR_SDF_pll_sdf)

# Prevent pulse swallowing at IO pads (according to ARM io manual)

VCS_POSTAPR_SDF_OPTIONS += +pathpulse

# IO pad settings in ns: letting SDF annotate over it..
# Note: IO invalid delay must not be 0 or the logic may not work correctly

#VCS_POSTAPR_SDF_OPTIONS += +define+ARM_PROP_IODELAY=0.000
#VCS_POSTAPR_SDF_OPTIONS += +define+ARM_IO_INVALID_DELAY=0.001
#VCS_POSTAPR_SDF_OPTIONS += +define+PULL=0.000

# This is a hierarchical design, and we need to make sure that any
# instances of each block correctly resolves to that block's gate-level
# netlist.
#
# Pull in each block's gate-level netlist into its own filelist and
# library. The config will specify that all instances of the block should
# use that library.

VCS_POSTAPR_SDF_OPTIONS += -f      $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/bnn.filelist
VCS_POSTAPR_SDF_OPTIONS += -libmap $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/bnn.library
VCS_POSTAPR_SDF_OPTIONS += -f      $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/coyote.filelist
VCS_POSTAPR_SDF_OPTIONS += -libmap $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/coyote.library
VCS_POSTAPR_SDF_OPTIONS += -f      $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/tile.filelist
VCS_POSTAPR_SDF_OPTIONS += -libmap $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/tile.library
VCS_POSTAPR_SDF_OPTIONS += -f      $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/manycore.filelist
VCS_POSTAPR_SDF_OPTIONS += -libmap $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/manycore.library
VCS_POSTAPR_SDF_OPTIONS += -f      $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/dcdctile.filelist
VCS_POSTAPR_SDF_OPTIONS += -libmap $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/dcdctile.library
VCS_POSTAPR_SDF_OPTIONS += -f      $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/dcdc_manycore.filelist
VCS_POSTAPR_SDF_OPTIONS += -libmap $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/dcdc_manycore.library
VCS_POSTAPR_SDF_OPTIONS += -f      $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/pll.filelist
VCS_POSTAPR_SDF_OPTIONS += -libmap $(VCS_POSTAPR_SDF_SCRIPTS_DIR)/pll.library

