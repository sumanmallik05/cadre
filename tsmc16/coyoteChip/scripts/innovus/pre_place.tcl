#setOptMode -fixFanoutLoad true
#setOptMode -setupTargetSlack 0.02

################################################################################
#                          PRE-PLACE PLUG-IN
################################################################################
#
# This plug-in script is called before placeDesign from the run_place.tcl flow
# script.
#
################################################################################
# Example tasks include:
#          - Power planning related tasks which includes
#            - Power planning for power domains (ring/strap creations)
#            - Power Shut-off cell power hookup
####################################################################################


################################################################################
# POWER CONFIGURATION PARAMETERS
################################################################################
# Lower grid has already been done by PGA


################################################################################
# POWER MESH CREATION
################################################################################

if {([string tolower $vars(use_pga)] != "true")} {
    source $::env(INNOVUS_FLOW_DIR)/common/tcl/simplePowerPlan.tcl
}



################################################################################
# WIRBOND
################################################################################

setFlipChipMode -connectPowerCellToBump true \
                -honor_bump_connect_target_constraint true \
                -ignore_pad_type_check true

fcroute -type signal \
        -designStyle pio \
        -layerChangeBotLayer AP \
        -layerChangeTopLayer AP

if {$vars(debug_power)} {
  saveDesign $vars(dbs_dir)/fcroute.enc -compress
}

# suspend
# deselectAll
# editSelect -net {VDD VSS} -layer {M1 M2 M3 M4 M5 M6 M7 M8 M9}
# editDelete -selected
# Limit tiehi / tielo fanout

setTieHiLoMode -maxDistance 20

# clock gating properties
set_ccopt_property clone_clock_gates true
set_ccopt_property ccopt_merge_clock_gates true
set_ccopt_property merge_clock_gates true
