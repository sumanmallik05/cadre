################################################################################
#                          PRE-PLACE PLUG-IN
################################################################################
#
# This plug-in script is called before placeDesign from the run_place.tcl flow
# script.
#
################################################################################
# Example tasks include:
#          - Power planning related tasks which includes
#            - Power planning for power domains (ring/strap creations)
#            - Power Shut-off cell power hookup
################################################################################


addRing -nets "VDD VSS" -type core_rings -follow core \
        -layer {top MQ bottom MQ left M4 right M4} \
        -width $p_rng_w -spacing $p_rng_s \
        -extend_corner {tl tr bl br lt lb rt rb} \
        -offset $p_rng_s
         # -center 1

source $vars(cadre,power_plan_tcl)


################################################################################
# POWER CONFIGURATION PARAMETERS
################################################################################
# Lower and upper grid has already been done by PGA or simplePowerPlan.tcl
