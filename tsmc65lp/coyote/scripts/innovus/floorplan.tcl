####################################################################################
#                             FLOORPLAN SCRIPT
####################################################################################

floorPlan -su $core_aspect_ratio \
             $core_density \
             $core_margin_l \
             $core_margin_b \
             $core_margin_r \
             $core_margin_t

setFlipping s

################################################################################
# MACRO PLACEMENT
################################################################################

set margin 3 ; # Set specifically to avoid dangling stripes
set spacing 4
set bigSpace [expr max([dbGet top.fplan.coreBox_sizex]/15, 30)]


#Left
placeInstance r2f/rocket/RocketTile/icache/icache/T198/mem/macro_mem         \
  [expr [dbGet top.fplan.coreBox_llx] + $margin] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R90

placeInstance r2f/rocket/RocketTile/icache/icache/tag_array/mem/macro_mem    \
  [expr [dbGet top.fplan.coreBox_llx] + $margin] \
  [expr [dbGet top.fplan.coreBox_ury] - $margin - [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/icache/icache/tag_array/mem/macro_mem].box_sizey]] \
  R0


placeInstance r2f/rocket/RocketTile/icache/icache/T226/mem/macro_mem         \
  [expr [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/icache/icache/T198/mem/macro_mem].box_urx] + $bigSpace] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R90
placeInstance r2f/rocket/RocketTile/icache/icache/T212/mem/macro_mem         \
  [expr [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/icache/icache/T226/mem/macro_mem].box_urx] + $spacing] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R90


placeInstance r2f/rocket/RocketTile/icache/icache/T239/mem/macro_mem         \
  [expr [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/icache/icache/T212/mem/macro_mem].box_urx] + $bigSpace] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R90



#Right
placeInstance r2f/rocket/RocketTile/dcache/data/T9/mem/macro_mem0             \
  [expr [dbGet top.fPlan.coreBox_urx] - $margin - [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T9/mem/macro_mem0].box_sizey]] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R270

placeInstance r2f/rocket/RocketTile/dcache/data/T9/mem/macro_mem1             \
  [expr [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T9/mem/macro_mem0].box_llx] - $spacing - [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T9/mem/macro_mem1].box_sizey]] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R270



placeInstance r2f/rocket/RocketTile/dcache/meta/tag_arr/mem/macro_mem0        \
  [expr [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T9/mem/macro_mem1].box_llx] - $bigSpace - [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/meta/tag_arr/mem/macro_mem0].box_sizey]] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R270
placeInstance r2f/rocket/RocketTile/dcache/meta/tag_arr/mem/macro_mem1        \
  [expr [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/meta/tag_arr/mem/macro_mem0].box_llx] - $spacing - [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/meta/tag_arr/mem/macro_mem1].box_sizey]] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R270


placeInstance r2f/rocket/RocketTile/dcache/data/T42/mem/macro_mem0            \
  [expr [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/meta/tag_arr/mem/macro_mem1].box_llx] - $bigSpace - [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T42/mem/macro_mem0].box_sizey]] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R270
placeInstance r2f/rocket/RocketTile/dcache/data/T42/mem/macro_mem1            \
  [expr [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T42/mem/macro_mem0].box_llx] - $spacing - [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T42/mem/macro_mem1].box_sizey]] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R270

placeInstance r2f/rocket/RocketTile/dcache/data/T79/mem/macro_mem0            \
  [expr [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T42/mem/macro_mem1].box_llx] - $bigSpace - [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T79/mem/macro_mem0].box_sizey]] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R270
placeInstance r2f/rocket/RocketTile/dcache/data/T79/mem/macro_mem1            \
  [expr [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T79/mem/macro_mem0].box_llx] - $spacing - [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T79/mem/macro_mem1].box_sizey]] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R270



placeInstance r2f/rocket/RocketTile/dcache/data/T112/mem/macro_mem0           \
  [expr [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T79/mem/macro_mem1].box_llx] - $bigSpace - [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T112/mem/macro_mem0].box_sizey]] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R270
placeInstance r2f/rocket/RocketTile/dcache/data/T112/mem/macro_mem1           \
  [expr [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T112/mem/macro_mem0].box_llx] - $spacing - [dbGet [dbGet -p1 top.insts.name r2f/rocket/RocketTile/dcache/data/T112/mem/macro_mem1].box_sizey]] \
  [expr [dbGet top.fplan.coreBox_lly] + $margin] \
  R270


selectInst [join "[dbGet -e [dbGet -p2 top.insts.cell.name $::env(PLATFORM)_*].name]"]
cutRow -selected -leftGap  $spacing -rightGap $spacing -topGap $spacing -bottomGap $spacing
deselectAll


editPin -fixOverlap 1 -fixedPin 1 -snap TRACK -spreadDirection clockwise -side Top -layer 6 -spreadType side \
-pin [join "
  [get_object_name [get_ports en_i]]
  [get_object_name [get_ports reset_i]]
  [get_object_name [get_ports rocc_cmd_data_o]]
  [get_object_name [get_ports rocc_cmd_ready_i]]
  [get_object_name [get_ports rocc_cmd_v_o]]
  [get_object_name [get_ports rocc_ctrl_i]]
  [get_object_name [get_ports rocc_ctrl_o]]
  [get_object_name [get_ports rocc_mem_req_data_i]]
  [get_object_name [get_ports rocc_mem_req_ready_o]]
  [get_object_name [get_ports rocc_mem_req_v_i]]
  [get_object_name [get_ports clk_i]]
  [get_object_name [get_ports rocc_mem_resp_data_o]]
  [get_object_name [get_ports rocc_mem_resp_v_o]]
  [get_object_name [get_ports rocc_resp_data_i]]
  [get_object_name [get_ports rocc_resp_ready_o]]
  [get_object_name [get_ports rocc_resp_v_i]]
"]

editPin -fixOverlap 1 -fixedPin 1 -snap TRACK -spreadDirection clockwise -side Bottom -layer 6 -spreadType side \
-pin [join "
  [get_object_name [get_ports fsb_node_data_i]]
  [get_object_name [get_ports fsb_node_data_o]]
  [get_object_name [get_ports fsb_node_ready_o]]
  [get_object_name [get_ports fsb_node_v_i]]
  [get_object_name [get_ports fsb_node_v_o]]
  [get_object_name [get_ports fsb_node_yumi_i]]
"]
