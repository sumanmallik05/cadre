setOptMode -fixFanoutLoad true


setCTSMode -optAddBuffer true -optLatency true
#set_ccopt_property post_conditioning_enable_skew_fixing true
#set_ccopt_property post_conditioning_enable_skew_fixing_by_rebuffering true
#set_ccopt_property post_conditioning_enable_skew_fixing_by_rebuffering_ccopt true
#set_ccopt_property post_conditioning_enable_drv_fixing_by_rebuffering true
#set_ccopt_property post_conditioning_enable_drv_fixing_by_rebuffering_ccopt true
#set_ccopt_property pro_enable_drv_fixing_by_rebuffering_second_pass true
set_ccopt_property effort high
#
#add_ndr -name CTS_3W2S -width_multiplier {M1:M9 3} -spacing_multiplier {M1:M9 2}
#add_ndr -name CTS_2W2S -width_multiplier {M1:M9 2} -spacing_multiplier {M1:M9 2}
#add_ndr -name CTS_1W2S -spacing_multiplier {M1:M9 2}
#
#create_route_type -name top_rule -non_default_rule CTS_2W2S -top_preferred_layer M7 -bottom_preferred_layer M6 -shield_net VSS -bottom_shield_layer M6
#
#create_route_type -name trunk_rule -non_default_rule CTS_2W2S -top_preferred_layer M7 -bottom_preferred_layer M6 -shield_net VSS -bottom_shield_layer M6
#
#create_route_type -name leaf_rule -non_default_rule CTS_1W2S -top_preferred_layer M3 -bottom_preferred_layer M2
#
#set_ccopt_property -net_type leaf route_type leaf_rule
#set_ccopt_property -net_type trunk route_type trunk_rule
#set_ccopt_property -net_type top route_type top_rule
