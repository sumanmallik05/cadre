################################################################################
#                          PRE-PLACE PLUG-IN
################################################################################
#
# This plug-in script is called before placeDesign from the run_place.tcl flow
# script.
#
################################################################################
# Example tasks include:
#          - Power planning related tasks which includes
#            - Power planning for power domains (ring/strap creations)
#            - Power Shut-off cell power hookup
################################################################################

source $::env(INNOVUS_FLOW_DIR)/common/tcl/addIoFiller.tcl


if {([string tolower $vars(use_pga)] != "true")} {
    source $::env(INNOVUS_FLOW_DIR)/common/tcl/simplePowerPlan.tcl
}


create_bump \
 -cell PBP40_18_18_NT_DR_MOD \
 -pitch "[expr $bondpad_width + $bondpad_spacing] $bondpad_height" \
 -edge_spacing "[expr $bondpad_height + $bondpad_margin] $bondpad_height [expr $bondpad_height + $bondpad_margin]  $bondpad_height" \
 -pattern_side {top 1} \
 -name_format Bondpad_N%i \
 -start_index 0 \
 -loc_type cell_lowerleft

create_bump \
 -cell PBP40_18_18_NT_DR_MOD \
 -pitch "[expr $bondpad_width + $bondpad_spacing] $bondpad_height" \
 -edge_spacing "[expr $bondpad_height + $bondpad_margin] 0 [expr $bondpad_height + $bondpad_margin] 0" \
 -pattern_side {bottom 1} \
 -name_format Bondpad_S%i \
 -start_index 0 \
 -loc_type cell_lowerleft

create_bump \
 -cell PBP40_18_18_NT_DR_MOD \
 -pitch "$bondpad_height [expr $bondpad_width + $bondpad_spacing]" \
 -edge_spacing "0 [expr $bondpad_height + $bondpad_margin] 0 [expr $bondpad_height + $bondpad_margin]" \
 -pattern_side {left 1} \
 -orientation R90 \
 -name_format Bondpad_W%i \
 -start_index 0 \
 -loc_type cell_lowerleft

create_bump \
 -cell PBP40_18_18_NT_DR_MOD \
 -pitch "$bondpad_height [expr $bondpad_width + $bondpad_spacing]" \
 -edge_spacing "0 [expr $bondpad_height + $bondpad_margin] $bondpad_height [expr $bondpad_height + $bondpad_margin]" \
 -pattern_side {right 1} \
 -orientation R90 \
 -name_format Bondpad_E%i \
 -start_index 0 \
 -loc_type cell_lowerleft

assignBump -pgnet {VDD DVDD VSS}

#               -route_style manhattan
setFlipChipMode -connectPowerCellToBump true \
                -honor_bump_connect_target_constraint true \
                -ignore_pad_type_check true \

#        -routeWidth 5.4
fcroute -type signal \
        -designStyle pio \
        -layerChangeBotLayer AP \
        -layerChangeTopLayer AP \

if {$vars(debug_power)} {
  saveDesign $vars(dbs_dir)/fcroute.enc -compress
}

# deselectAll
# editSelect -net {VDD VSS} -layer {M1 M2 M3 M4 M5 M6 M7 M8 M9}
# editDelete -selected

################################################################################
# POWER CONFIGURATION PARAMETERS
################################################################################
# Lower and upper grid has already been done by PGA or simplePowerPlan.tcl
