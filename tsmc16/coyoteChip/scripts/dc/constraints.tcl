puts "Info: Running script [info script]\n"
set_cost_priority -min_delay

#Make all signals meet good slew
set_max_transition 100 ${DESIGN_NAME}
set_input_transition 69 [all_inputs]
set_max_transition 69 [all_outputs]

#Make all signals limit their fanout
set_max_fanout 10 ${DESIGN_NAME}

set clock_period           1000
set clock_skew_internal      30
set clock_skew_external      10
set clock_transition         69
set clock_jitter              0.05

# Set clock and reasonable driving strength
create_clock -name core_clk -period $clock_period [get_ports {PAD_E[0]}]
set_clock_uncertainty $clock_skew_internal [get_clocks core_clk]

set_clock_latency -source -dynamic [expr ($clock_jitter * $clock_period)] 0 [get_clocks core_clk]

set_clock_transition $clock_transition [get_clocks core_clk]

set_load 3.0 [all_outputs]

# From spreadsheet
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_E[8]}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_S[3] PAD_S[2] PAD_S[1] PAD_S[0]}] -clock core_clk
set_output_delay [expr ($clock_period * 0.55) + $clock_skew_external] [get_ports {PAD_E[10]}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_E[11]}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_S[7] PAD_S[6] PAD_S[5] PAD_S[4]}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_S[11] PAD_S[10] PAD_S[9] PAD_S[8]}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_E[9]}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_E[12]}] -clock core_clk
set_output_delay [expr ($clock_period * 0.60) + $clock_skew_external] [get_ports {PAD_E[13]}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_S[15] PAD_S[14] PAD_S[13] PAD_S[12]}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_E[3]}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_E[4]}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_W[0] PAD_W[1] PAD_W[2] PAD_W[3] PAD_W[4] PAD_W[5] PAD_W[6] PAD_W[7]}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.60) + $clock_skew_external] [get_ports {PAD_E[5]}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.55) + $clock_skew_external] [get_ports {PAD_W[8] PAD_W[9] PAD_W[10] PAD_W[11] PAD_W[12] PAD_W[13] PAD_W[14] PAD_W[15]}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.55) + $clock_skew_external] [get_ports {PAD_N[0] PAD_N[1]}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_E[6]}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_N[2] PAD_N[3] PAD_N[4] PAD_N[5] PAD_N[6] PAD_N[7] PAD_N[8] PAD_N[9] PAD_N[10] PAD_N[11] PAD_N[12] PAD_N[13] PAD_N[14] PAD_N[15]}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.60) + $clock_skew_external] [get_ports {PAD_E[7]}] -clock core_clk

# Handle reset seperately
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_E[1]}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {PAD_E[2]}] -clock core_clk

# rocc_ctrl[interrupt] is a static zero
#set_false_path -from [get_ports {rocc_ctrl_i[interrupt]}]
set_logic_zero [get_ports {PAD_N[1] PAD_N[0]}]

# rocc_resp_data_i[32:64] is unused across all rockets
#set_false_path -from [get_ports -regexp {rocc_resp_data_i\\[data\\]\\[(3[2-9]|[4-5][0-9]|6[0-3])\\]}]
# set_logic_zero [get_ports PBIDIRN_W_Y[7:0]]
#TODO this could probably be set_unconnected
set_false_path -to [get_ports {PAD_S[11] PAD_S[9] PAD_S[8]}]

#Retime the FPU registers
set_optimize_registers true -design IntToFP      -clock core_clk -check_design -verbose -delay_threshold ${clock_period} -print_critical_loop
set_optimize_registers true -design FPToFP       -clock core_clk -check_design -verbose -delay_threshold ${clock_period} -print_critical_loop
set_optimize_registers true -design FPUFMAPipe_0 -clock core_clk -check_design -verbose -delay_threshold ${clock_period} -print_critical_loop
set_optimize_registers true -design FPUFMAPipe_1 -clock core_clk -check_design -verbose -delay_threshold ${clock_period} -print_critical_loop

puts "Info: Completed script [info script]\n"
