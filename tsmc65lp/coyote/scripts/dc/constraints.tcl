puts "Info: Running script [info script]\n"
set_cost_priority -min_delay

#Make all signals meet good slew
set_max_transition 0.100 ${DESIGN_NAME}
set_input_transition 0.069 [all_inputs]
set_max_transition 0.069 [all_outputs]

#Make all signals limit their fanout
set_max_fanout 10 ${DESIGN_NAME}

set clock_period             5
set clock_skew_internal      0.030
set clock_skew_external      0.010
set clock_transition         0.069
set clock_jitter             0.00005

# Set clock and reasonable driving strength
create_clock -name core_clk -period $clock_period [get_ports clk_i]
set_clock_uncertainty $clock_skew_internal [get_clocks core_clk]

set_clock_latency -source -dynamic [expr ($clock_jitter * $clock_period)] 0 [get_clocks core_clk]

set_clock_transition $clock_transition [get_clocks core_clk]

set_load 3.0 [all_outputs]

# From spreadsheet
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {rocc_cmd_v_o}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {rocc_cmd_data_o*}] -clock core_clk
set_output_delay [expr ($clock_period * 0.55) + $clock_skew_external] [get_ports {rocc_mem_req_ready_o}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {rocc_mem_resp_v_o}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {rocc_mem_resp_data_o*}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {rocc_ctrl_o*}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {rocc_resp_ready_o}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {fsb_node_ready_o}] -clock core_clk
set_output_delay [expr ($clock_period * 0.60) + $clock_skew_external] [get_ports {fsb_node_v_o}] -clock core_clk
set_output_delay [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {fsb_node_data_o}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {rocc_cmd_ready_i}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {rocc_resp_v_i}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {rocc_resp_data_i*}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.60) + $clock_skew_external] [get_ports {rocc_mem_req_v_i}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.55) + $clock_skew_external] [get_ports {rocc_mem_req_data_i*}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.55) + $clock_skew_external] [get_ports {rocc_ctrl_i*}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {fsb_node_v_i}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {fsb_node_data_i}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.60) + $clock_skew_external] [get_ports {fsb_node_yumi_i}] -clock core_clk

# Handle reset seperately
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {reset_i}] -clock core_clk
set_input_delay  [expr ($clock_period * 0.50) + $clock_skew_external] [get_ports {en_i}] -clock core_clk

# rocc_ctrl[interrupt] is a static zero
#set_false_path -from [get_ports {rocc_ctrl_i[interrupt]}]
set_logic_zero [get_ports {rocc_ctrl_i[interrupt]}]

# rocc_resp_data_i[32:64] is unused across all rockets
#set_false_path -from [get_ports -regexp {rocc_resp_data_i\\[data\\]\\[(3[2-9]|[4-5][0-9]|6[0-3])\\]}]
set_logic_zero [get_ports -regexp {rocc_resp_data_i\\[data\\]\\[(3[2-9]|[4-5][0-9]|6[0-3])\\]}]
#TODO this could probably be set_unconnected
set_false_path -to [get_ports {rocc_ctrl_o*}]

#Retime the FPU registers
set_optimize_registers true -design IntToFP      -clock core_clk -check_design -verbose -delay_threshold ${clock_period} -print_critical_loop
set_optimize_registers true -design FPToFP       -clock core_clk -check_design -verbose -delay_threshold ${clock_period} -print_critical_loop
set_optimize_registers true -design FPUFMAPipe_0 -clock core_clk -check_design -verbose -delay_threshold ${clock_period} -print_critical_loop
set_optimize_registers true -design FPUFMAPipe_1 -clock core_clk -check_design -verbose -delay_threshold ${clock_period} -print_critical_loop

puts "Info: Completed script [info script]\n"
