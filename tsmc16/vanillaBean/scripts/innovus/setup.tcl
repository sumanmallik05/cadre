set vars(max_route_layer) 7
set vars(cpf_file)        "$::env(FF_DESIGN_SCRIPTS_DIR)/power_intent.cpf"

set vars(debug_power) true

if {$::env(SITE)=="ucsd"} {
  set vars(signoff,extract_rc,skip) true
  set vars(signoff,dump_spef,skip) true
  set vars(signoff,time_design_setup,skip) true
  set vars(signoff,time_design_hold,skip) true
}