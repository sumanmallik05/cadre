####################################################################################
#                              ALWAYS_SOURCE PLUG-IN
#####################################################################################
#
# This plug-in script is called from all flow scripts after loading the setup.tcl
# but after to loading the design data.  It can be used to set variables that affect
# non-persistent information
#
#####################################################################################
set t_pitch [dbGet top.fPlan.coreSite.size_y]     ;# Pitch between power rails (standard cell height)
set f_pitch [dbGet head.finGridPitch]             ;# Pitch between fins

# Variables used by Power Planning scripts
set pwr_net_list {VDD VSS}              ;# List of Power nets

set p_rng_w      0.960 ; # DRC rule required two at 0.960
set p_rng_s      0.480; # DRC rule violation with min at 1.35
set p_str_w      0.960; # Power stripe metal width
set p_str_s      0.720; # Power stripe metal space
#set p_rng_off  [expr 0.480 * 10]; # Arbitrarily increased to reduce MinCut violations

set p_str_p      9.6     ;# Power stripe metal pitch
set p_str_m      7.2     ;# Power stripe metal margin

# Floorplan Variables
# All BOX widths must be a multiple of fin pitch and height a multiple of track pitch
set p_str_margin  [expr ([llength $pwr_net_list] * ($p_str_w + $p_str_s)) + $p_str_s]
set core_margin_t [expr ([llength $pwr_net_list] * ($p_rng_w + $p_rng_s)) + $p_rng_s]
set core_margin_b [expr ([llength $pwr_net_list] * ($p_rng_w + $p_rng_s)) + $p_rng_s]
set core_margin_r [expr ([llength $pwr_net_list] * ($p_rng_w + $p_rng_s)) + $p_rng_s]
set core_margin_l [expr ([llength $pwr_net_list] * ($p_rng_w + $p_rng_s)) + $p_rng_s]

set core_width   [expr 15120 * $f_pitch ] ;# Core Area Width    #720um
set core_height  [expr   550 * $t_pitch ] ;# Core Area Height   #412.8um

# Margin around srams
set mem_macros [ dbGet top.insts.cell.name *tsmc* -p2 ]
set sram_margin [expr $p_str_margin * 0.5]

