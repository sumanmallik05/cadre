# File order matters
set SVERILOG_SOURCE_FILES "
../vanillaBean/src/verilog/alu.v
../vanillaBean/src/verilog/bsg_adder_cin.v
../vanillaBean/src/verilog/bsg_arb_fixed.v
../vanillaBean/src/verilog/bsg_buf.v
../vanillaBean/src/verilog/bsg_buf_ctrl.v
../vanillaBean/src/verilog/bsg_circular_ptr.v
../vanillaBean/src/verilog/bsg_counter_up_down.v
../vanillaBean/src/verilog/bsg_crossbar_o_by_i.v
../vanillaBean/src/verilog/bsg_dff_en.v
../vanillaBean/src/verilog/bsg_dff_reset.v
../vanillaBean/src/verilog/bsg_dff_reset_en.v
../vanillaBean/src/verilog/bsg_fifo_1r1w_small.v
../vanillaBean/src/verilog/bsg_fifo_tracker.v
../vanillaBean/src/verilog/bsg_idiv_iterative.v
../vanillaBean/src/verilog/bsg_idiv_iterative_controller.v
../vanillaBean/src/verilog/bsg_imul_iterative.v
../vanillaBean/src/verilog/bsg_manycore_endpoint.v
../vanillaBean/src/verilog/bsg_manycore_endpoint_standard.v
../vanillaBean/src/verilog/bsg_manycore_hetero_socket.v
../vanillaBean/src/verilog/bsg_manycore_mesh_node.v
../vanillaBean/src/verilog/bsg_manycore_pkt_decode.v
../vanillaBean/src/verilog/bsg_manycore_pkt_encode.v
../vanillaBean/src/verilog/bsg_manycore_proc_vanilla.v
../vanillaBean/src/verilog/bsg_manycore_tile.v
../vanillaBean/src/verilog/bsg_mem_1r1w.v
../vanillaBean/src/verilog/bsg_mem_1r1w_synth.v
../vanillaBean/src/verilog/bsg_mem_1rw_sync.v
../vanillaBean/src/verilog/bsg_mem_1rw_sync_mask_write_byte.v
../vanillaBean/src/verilog/bsg_mem_2r1w_sync.v
../vanillaBean/src/verilog/bsg_mem_banked_crossbar.v
../vanillaBean/src/verilog/bsg_mesh_router.v
../vanillaBean/src/verilog/bsg_mesh_router_buffered.v
../vanillaBean/src/verilog/bsg_mux.v
../vanillaBean/src/verilog/bsg_mux_one_hot.v
../vanillaBean/src/verilog/bsg_nor2.v
../vanillaBean/src/verilog/bsg_priority_encode_one_hot_out.v
../vanillaBean/src/verilog/bsg_round_robin_arb.v
../vanillaBean/src/verilog/bsg_scan.v
../vanillaBean/src/verilog/bsg_transpose.v
../vanillaBean/src/verilog/bsg_two_fifo.v
../vanillaBean/src/verilog/bsg_xnor.v
../vanillaBean/src/verilog/cl_decode.v
../vanillaBean/src/verilog/cl_state_machine.v
../vanillaBean/src/verilog/hobbit.v
../vanillaBean/src/verilog/imul_idiv_iterative.v
../vanillaBean/src/verilog/rf_2r1w_sync_wrapper.v

./src/verilog/bsg_manycore_tile_openroad.v

"
set NETLIST_SOURCE_FILES ""
