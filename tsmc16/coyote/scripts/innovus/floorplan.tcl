####################################################################################
#                             FLOORPLAN SCRIPT
####################################################################################

floorPlan -s $core_width \
             $core_height \
             $core_margin_l \
             $core_margin_b \
             $core_margin_r \
             $core_margin_t

setFlipping s

################################################################################
# MACRO PLACEMENT
################################################################################
# Mimimum memory spacings from ARM sram_sp_hde README

# Minimum memory to stdcell spacing
set min_mem_stdcell_spacing_h 0.540
set min_mem_stdcell_spacing_v 0.672

# Minimum memory to memory spacing
# NOTE: identical memory macros can be abutted on the non-pin edge
set min_mem_mem_spacing_h     1.440
set min_mem_mem_spacing_v     0.672

# Define cell sizes
# Dcache
set mem1_x [dbGet [dbGetCellByName tsmc16_1r1w_lg6_w88_bit].size_x]
set mem1_y [dbGet [dbGetCellByName tsmc16_1r1w_lg6_w88_bit].size_y]

set mem2_x [dbGet [dbGetCellByName tsmc16_1r1w_lg8_w128_bit].size_x]
set mem2_y [dbGet [dbGetCellByName tsmc16_1r1w_lg8_w128_bit].size_y]

#Icache
set rf1_x  [dbGet [dbGetCellByName tsmc16_1rf_lg6_w80_bit].size_x]
set rf1_y  [dbGet [dbGetCellByName tsmc16_1rf_lg6_w80_bit].size_y]

set rf2_x  [dbGet [dbGetCellByName tsmc16_1rf_lg8_w128_all].size_x]
set rf2_y  [dbGet [dbGetCellByName tsmc16_1rf_lg8_w128_all].size_y]

#FIFO
set rf3_x  [dbGet [dbGetCellByName tsmc16_1rf_lg7_w74_all].size_x]
set rf3_y  [dbGet [dbGetCellByName tsmc16_1rf_lg7_w74_all].size_y]
# Print all macros:
#
dbGet $mem_macros.name

set spacing 0
set sram_x_left [expr $core_margin_l + $spacing]
set sram_x_right [expr $core_margin_l + $core_width]
#Left
placeInstance r2f/rocket/RocketTile/icache/icache/tag_array/mem/macro_mem    [expr $sram_x_left]          [expr $core_margin_b + $rf2_y + $min_mem_mem_spacing_v] My;        #tsmc16_1rf_lg6_w80_bit
placeInstance r2f/rocket/RocketTile/icache/icache/T198/mem/macro_mem         [expr $sram_x_left]                                  [expr $core_margin_b] My;        #tsmc16_1rf_lg8_w128_all
placeInstance r2f/rocket/RocketTile/icache/icache/T212/mem/macro_mem         [expr $sram_x_left + $core_width/7]                  [expr $core_margin_b] My;        #tsmc16_1rf_lg8_w128_all
placeInstance r2f/rocket/RocketTile/icache/icache/T226/mem/macro_mem         [expr $sram_x_left + $core_width/7 -$rf2_x]          [expr $core_margin_b]   ;        #tsmc16_1rf_lg8_w128_all
placeInstance r2f/rocket/RocketTile/icache/icache/T239/mem/macro_mem         [expr $sram_x_left + $core_width/6 +2*$rf2_x]          [expr $core_margin_b]   ;        #tsmc16_1rf_lg8_w128_all
#placeInstance n_0__clnt/r2f/tunnel/bcti/b1_ntf/rof_0__psdlrg_fifo/big1p/mem_1srw/macro_mem       [expr $sram_x_left] [expr $core_margin_b + 4*$rf2_y + $rf1_y + 5*60] My; #tsmc16_1rf_lg7_w74_all

#Right
placeInstance r2f/rocket/RocketTile/dcache/meta/tag_arr/mem/macro_mem        [expr $sram_x_right - $mem1_x] [expr $core_margin_b + $rf2_y + $min_mem_mem_spacing_v]            ;        #tsmc16_1r1w_lg6_w88_bit
placeInstance r2f/rocket/RocketTile/dcache/data/T9/mem/macro_mem             [expr $sram_x_right - $mem2_x]                          [expr $core_margin_b]   ;        #tsmc16_1r1w_lg8_w128_bit
placeInstance r2f/rocket/RocketTile/dcache/data/T42/mem/macro_mem            [expr $sram_x_left + (3 * $core_width) / 4]             [expr $core_margin_b] My;        #tsmc16_1r1w_lg8_w128_bit
placeInstance r2f/rocket/RocketTile/dcache/data/T79/mem/macro_mem            [expr $sram_x_left + (3 * $core_width) / 4 -$mem2_x]    [expr $core_margin_b]   ;        #tsmc16_1r1w_lg8_w128_bit
placeInstance r2f/rocket/RocketTile/dcache/data/T112/mem/macro_mem           [expr $sram_x_left + 11*$core_width/20]                     [expr $core_margin_b] My;        #tsmc16_1r1w_lg8_w128_bit
#placeInstance n_0__clnt/r2f/tunnel/bcti/b1_ntf/rof_1__psdlrg_fifo/big1p/mem_1srw/macro_mem        [expr $sram_x_right - $rf3_x]  [expr $core_margin_b + 4*$mem2_y + $mem1_y + 5*60]; #tsmc16_1rf_lg7_w74_all

selectInst "$mem_macros"
snapFPlan -selected
cutRow -selected -halo $sram_margin
deselectInst *

loadIoFile $::env(FF_DESIGN_SCRIPTS_DIR)/$::env(DESIGN_NAME).save.io
