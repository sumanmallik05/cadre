module bsg_rocket_node_client_rocc (
	clk_i, 
	reset_i, 
	en_i, 
	rocc_cmd_v_o, 
	rocc_cmd_data_o, 
	rocc_cmd_ready_i, 
	rocc_resp_v_i, 
	rocc_resp_data_i, 
	rocc_resp_ready_o, 
	rocc_mem_req_v_i, 
	rocc_mem_req_data_i, 
	rocc_mem_req_ready_o, 
	rocc_mem_resp_v_o, 
	rocc_mem_resp_data_o, 
	rocc_ctrl_i, 
	rocc_ctrl_o, 
	fsb_node_v_i, 
	fsb_node_data_i, 
	fsb_node_ready_o, 
	fsb_node_v_o, 
	fsb_node_data_o, 
	fsb_node_yumi_i);
   input clk_i;
   input reset_i;
   input en_i;
   output rocc_cmd_v_o;
   output [159:0] rocc_cmd_data_o;
   input rocc_cmd_ready_i;
   input rocc_resp_v_i;
   input [68:0] rocc_resp_data_i;
   output rocc_resp_ready_o;
   input rocc_mem_req_v_i;
   input [122:0] rocc_mem_req_data_i;
   output rocc_mem_req_ready_o;
   output rocc_mem_resp_v_o;
   output [252:0] rocc_mem_resp_data_o;
   input [1:0] rocc_ctrl_i;
   output [2:0] rocc_ctrl_o;
   input fsb_node_v_i;
   input [79:0] fsb_node_data_i;
   output fsb_node_ready_o;
   output fsb_node_v_o;
   output [79:0] fsb_node_data_o;
   input fsb_node_yumi_i;

endmodule
