####################################################################################
#                              ALWAYS_SOURCE PLUG-IN
#####################################################################################
#
# This plug-in script is called from all flow scripts after loading the setup.tcl
# but after to loading the design data.  It can be used to set variables that affect
# non-persistent information
#
#####################################################################################
set t_pitch [lindex [lsort -real [dbGet head.libcells.size_y]] 0];# Pitch between power rails (standard cell height)
                                               # TODO Currently a hacky way of getting it; there should be a better way
                                               # dbHeadStdCellHgt does not give correct height in tsmc16
set f_pitch [dbGet head.finGridPitch]         ;# Pitch between fins

# Floorplan Variables
set core_width   [expr 50 * $t_pitch] ;# Core Area Width
set core_height  [expr 50 * $t_pitch] ;# Core Area Height

set coreBox_llx [dbGet top.fplan.coreBox_llx]
set coreBox_lly [dbGet top.fplan.coreBox_lly]
set coreBox_urx [dbGet top.fplan.coreBox_urx]
set coreBox_ury [dbGet top.fplan.coreBox_ury]

set pwr_net_list {VDD VSS}              ;# List of Power nets

set p_rng_w      1.8                  ;# Power ring metal width
set p_rng_s      1.2                  ;# Power ring metal space

set p_str_w      0.4                  ;# Power stripe metal width
set p_str_s      1.6                  ;# Power stripe metal space
set p_str_p      16                   ;# Power stripe metal pitch

set p_str_margin  [expr ([llength $pwr_net_list] * ($p_str_w + $p_str_s)) + $p_str_s]
set core_margin_t [expr ([llength $pwr_net_list] * ($p_rng_w + $p_rng_s)) + $p_rng_s]
set core_margin_b [expr ([llength $pwr_net_list] * ($p_rng_w + $p_rng_s)) + $p_rng_s]
set core_margin_r [expr ([llength $pwr_net_list] * ($p_rng_w + $p_rng_s)) + $p_rng_s]
set core_margin_l [expr ([llength $pwr_net_list] * ($p_rng_w + $p_rng_s)) + $p_rng_s]

set die_width     [expr $core_width  + $core_margin_l + $core_margin_r]
set die_height    [expr $core_height + $core_margin_b + $core_margin_t]
