#!/usr/bin/env python

import argparse  # argument parsing
import json  # json parsing
import sys
import re
import os  # filesystem manipulation
import shutil  # File Manipulation
import xml.etree.ElementTree as ET
from collections import OrderedDict


# Parse and validate arguments
# ==============================================================================
parser = argparse.ArgumentParser(
    description='Generates idf from innovus floorplans')
parser.add_argument('--floorPlanFile', '-f', required=True,
                    help='Path to the *.fp file describing the design')
parser.add_argument('--outputDir', '-o', required=True,
                    help='Output directory for generated design')
parser.add_argument('--constraint', '-c', required=True,
                    help='Name of the constraints to be used for the design')
parser.add_argument('--platform', '-p', required=True,
                    help='platform')
parser.add_argument('--verilog', '-v', required=True, nargs=argparse.REMAINDER,
                    help='Name of the Verilog file(s) for the design')
args = parser.parse_args()

if not os.path.isfile(args.floorPlanFile):
  print("Error: floorPlanFile does not exist")
  print("File Path: " + args.floorPlanFile)
  sys.exit(1)

if args.platform != "tsmc16":
  print("Error: tsmc16 is the only platform supported")
  sys.exit(1)



# ==============================================================================

# Open file
with open(args.floorPlanFile) as f:
  content = f.read()

jsonFile = OrderedDict()
jsonFile["comment"] = "Auto-generated"
jsonFile["version"] = 0.3
jsonFile["units"] = {
    "x": "track_width",
    "y": "track_hieght"}


# Get design name
pattern = "^#  Design: +(\S+)"
m = re.search(pattern, content, re.M)
if m:
  jsonFile["toplevel"] = m.group(1)
else:
  print("Design name not found")
  sys.exit(1)


# get area
pattern = "Head Box: (\S+) (\S+) (\S+) (\S+)"
m = re.search(pattern, content)
if m:
  jsonFile["area"] = {
      "die": {
          "llx": float(m.group(1)),
          "lly": float(m.group(2)),
          "urx": float(m.group(3)),
          "ury": float(m.group(4))}
  }
else:
  print("Area not found")
  sys.exit(1)

pattern = "Core Box: (\S+) (\S+) (\S+) (\S+)"
m = re.search(pattern, content)
if m:
  jsonFile["area"].update({
      "core": {
          "llx": float(m.group(1)),
          "lly": float(m.group(2)),
          "urx": float(m.group(3)),
          "ury": float(m.group(4))}
  }
  )
else:
  print("Area not found")
  sys.exit(1)


pattern = "\n<IOPins>.*</IOPins>"
m = re.search(pattern, content, re.DOTALL | re.M)
if m:
  iosection = m.group(0)
else:
  print("Design name not found")
  sys.exit(1)


iosection = re.sub("=([^\" ]+)", r'="\1"', iosection)
# iosection = "<?xml version=\"1.0\"?>\n" + re.sub("=([^\" ]+)", r'="\1"', iosection)


IoPins = ET.fromstring(iosection)


jsonFile["designs"] = [OrderedDict({"name": jsonFile["toplevel"]})]
jsonFile["designs"][0]["io_ports"] = []

for pin in IoPins:
  for port in pin:
    for layer in port:  # or Pref
      for box in layer:
        # print pin.attrib["name"], port.find("Pref").attrib["side"],
        # layer.attrib["id"], box.attrib["llx"], box.attrib["lly"]
        io_port = OrderedDict()
        io_port["name"] = pin.attrib["name"]
        io_port["x"] = box.attrib["llx"]
        io_port["y"] = box.attrib["lly"]
        io_port["side"] = port.find("Pref").attrib["side"]
        io_port["layer"] = layer.attrib["id"]
        jsonFile["designs"][0]["io_ports"].append(io_port)

jsonFile["designs"][0]["place"] = []
jsonFile["designs"][0]["placement_bounds"] = []
jsonFile["designs"][0]["sensitive_nets"] = []


for m in re.finditer("^Block: (\S+) (\S+) (\S+) (\S+)", content, re.M):
  place_entry = OrderedDict()
  place_entry["name"] = m.group(1)
  place_entry["type"] = "?"
  place_entry["x"] = m.group(3)
  place_entry["y"] = m.group(4)
  place_entry["orientation"] = m.group(2)

  jsonFile["designs"][0]["place"].append(place_entry)


outputFile = args.outputDir + "/" + \
           os.path.splitext(os.path.basename(args.floorPlanFile))[0] + \
           ".idf.json"

with open(outputFile, "w") as resultSpecfile:
  json.dump(jsonFile, resultSpecfile, indent=2)

shutil.copyfile(args.constraint, args.outputDir + "/" + os.path.basename(args.constraint))
for file in args.verilog:
  shutil.copyfile(file, args.outputDir + "/" + os.path.basename(file))


