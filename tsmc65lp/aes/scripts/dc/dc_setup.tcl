puts "Info: Running script [info script]\n"

# Either use the //synopsys sync_set_reset "reset, int_reset" pragma or set the
# hdlin_ff_always_sync_set_reset variable to true. Using the pragma enables HDL
# Compiler to recognize the "reset" and "int_reset" signals (in the case of the
# sync_set_reset pragma) as reset signals and to create the reset logic
# appropriately. Setting the hdlin_ff_always_sync_set_reset variable instructs
# HDL Compiler to treat all resets are synchronous and to look for synchronous
# resets in the design.
#
# By default, the hdlin_ff_always_async_set_reset variable is set to true,
# and the hdlin_ff_always_sync_set_reset variable is set to false.
#
set hdlin_ff_always_sync_set_reset true

# Prevent removing constant registers.
# This options is in old synthesis script.
# set compile_seqmap_propagate_constants false

# Grab the search paths for the bsg_design (defines SVERILOG_INCLUDE_PATHS)
source $::env(DC_SCRIPTS_DIR)/dc.include.tcl

# Print one file per line
redirect -tee $::env(DC_LOGS_DIR)/sverilog_includeList_dc.txt {puts "[join $SVERILOG_INCLUDE_PATHS \n]\n"}

# Add bsg include paths to the serach_path
set_app_var search_path "${SVERILOG_INCLUDE_PATHS} $search_path"

set analysis_type single_typical

# This designs fails to write a system verilog wrapper. Supressing the error
set suppress_errors {VO-19}


puts "Info: Completed script [info script]\n"
