Generated using OpenRAM. Manual changes include:
1. lib: Changed voltage units from "v" to "V"
1. lib: Lowercase pins for CSb0 WEb0
1. db: Generate db using lc_shell
1. lef: Regenerate lef file from gds using virtuoso
