#setOptMode -fixFanoutLoad true
#setOptMode -setupTargetSlack 0.02

################################################################################
#                          PRE-PLACE PLUG-IN
################################################################################
#
# This plug-in script is called before placeDesign from the run_place.tcl flow
# script.
#
################################################################################
# Example tasks include:
#          - Power planning related tasks which includes
#            - Power planning for power domains (ring/strap creations)
#            - Power Shut-off cell power hookup
####################################################################################

######
#In case we want to overprovision slack (positive number) or
# force it to focus on ones that well exceed the negative number
######
#RGD:  Signoff keeps loosing about 18ps of setup time, so adding in a user defined buffer for earlier stages





################################################################################
# POWER MESH CREATION
################################################################################


# Create a core ring for connectivity
# NOTE: We no longer use a power ring at the block level
# addRing -nets {VDD VSS} -type core_rings -follow core \
#     -layer {top M8 bottom M8 left M9 right M9} \
#     -width $p_rng_w -spacing $p_rng_s -offset $p_rng_s \
#     -extend_corner {tl tr bl br lt lb rt rb}


if {([string tolower $vars(use_pga)] != "true")} {
    source $::env(INNOVUS_FLOW_DIR)/common/tcl/simplePowerPlan.tcl
}


# Limit tiehi / tielo fanout
setTieHiLoMode -maxDistance 20

# clock gating properties
set_ccopt_property clone_clock_gates true
set_ccopt_property ccopt_merge_clock_gates true
set_ccopt_property merge_clock_gates true
