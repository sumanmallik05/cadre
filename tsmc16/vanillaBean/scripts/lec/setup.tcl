
setenv BSG_DESIGNS_TARGET_DIR "$::env(BSG_DESIGNS_DIR)/toplevels/bsg_two_manycore_vanilla"

source "$::env(BSG_DESIGNS_TARGET_DIR)/tcl/include.tcl"
source "$::env(BSG_DESIGNS_TARGET_DIR)/tcl/filelist.tcl"

set vars(parameters) "-parameter x_cord_width_p 1 -parameter y_cord_width_p 3 -parameter data_width_p 32 -parameter addr_width_p 16 -parameter debug_p 0 -parameter bank_size_p 1024 -parameter num_banks_p 1 -parameter imem_size_p 1024 -parameter max_out_credits_p 200 -parameter hetero_type_p 32'h00000000"

set vars(bbox) "tsmc16_1rw_lg10_w32_all tsmc16_1rw_lg10_w32_byte tsmc16_2rf_lg5_w32_all"
