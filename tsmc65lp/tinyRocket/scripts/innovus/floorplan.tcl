####################################################################################
#                             FLOORPLAN SCRIPT
####################################################################################

floorPlan -su $core_aspect_ratio \
             $core_density \
             $core_margin_l \
             $core_margin_b \
             $core_margin_r \
             $core_margin_t

setFlipping s

################################################################################
# MACRO PLACEMENT
################################################################################

set spacing 3 ; # Set specifically to avoid dangling stripes

###

placeInstance frontend/icache/data_arrays_0/data_arrays_0_0_ext/mem \
  [expr $spacing + [dbGet top.fPlan.coreBox_llx]] \
  [expr [dbGet top.fPlan.coreBox_ury] - $spacing - [dbGet [dbGet -p1 top.insts.name frontend/icache/data_arrays_0/data_arrays_0_0_ext/mem].box_sizey]] \
  R0

placeInstance dcache/data/data_arrays_0/data_arrays_0_ext/mem \
  [expr [dbGet top.fPlan.coreBox_urx] - $spacing - [dbGet [dbGet -p1 top.insts.name dcache/data/data_arrays_0/data_arrays_0_ext/mem].box_sizex]] \
  [expr [dbGet top.fPlan.coreBox_ury] - $spacing - [dbGet [dbGet -p1 top.insts.name dcache/data/data_arrays_0/data_arrays_0_ext/mem].box_sizey]] \
  R0



selectInst [join "[dbGet -e [dbGet -p2 top.insts.cell.name $::env(PLATFORM)_*].name]"]
cutRow -selected -leftGap  $spacing -rightGap $spacing -topGap $spacing -bottomGap $spacing
deselectAll

# source $::env(INNOVUS_FLOW_DIR)/common/tcl/planDesign.tcl

################################################################################
# I/O PIN PLACEMENT
################################################################################
editPin -fixOverlap 1 \
 -fixedPin 1 \
 -snap TRACK \
 -spreadDirection clockwise \
 -side Top \
 -layer 6 \
 -spreadType side \
 -pin [get_object_name [get_ports "auto_int_in_xing_in_0_sync_0
                                   auto_int_in_xing_in_0_sync_1
                                   auto_int_in_xing_in_1_sync_0
                                   auto_intsink_in_sync_0
                                   auto_tl_master_xing_out_a_bits_corrupt
                                   auto_tl_master_xing_out_a_bits_source
                                   auto_tl_master_xing_out_a_ready
                                   auto_tl_master_xing_out_a_valid
                                   auto_tl_master_xing_out_d_bits_corrupt
                                   auto_tl_master_xing_out_d_bits_denied
                                   auto_tl_master_xing_out_d_bits_sink
                                   auto_tl_master_xing_out_d_bits_source
                                   auto_tl_master_xing_out_d_ready
                                   auto_tl_master_xing_out_d_valid
                                   auto_tl_slave_xing_in_a_ready
                                   auto_tl_slave_xing_in_a_valid
                                   auto_tl_slave_xing_in_d_bits_corrupt
                                   auto_tl_slave_xing_in_d_bits_denied
                                   auto_tl_slave_xing_in_d_bits_sink
                                   auto_tl_slave_xing_in_d_ready
                                   auto_tl_slave_xing_in_d_valid
                                   clock
                                   reset
                                   auto_tl_master_xing_out_a_bits_address"]]

editPin -fixOverlap 1 \
 -fixedPin 1 \
 -snap TRACK \
 -spreadDirection clockwise \
 -side Right \
 -layer 5 \
 -spreadType side \
 -pin [get_object_name [get_ports "auto_tl_master_xing_out_a_bits_data
                                   auto_tl_master_xing_out_a_bits_mask
                                   auto_tl_master_xing_out_a_bits_opcode
                                   auto_tl_master_xing_out_a_bits_param
                                   auto_tl_slave_xing_in_d_bits_opcode
                                   auto_tl_slave_xing_in_d_bits_param
                                   auto_tl_slave_xing_in_d_bits_size
                                   auto_tl_slave_xing_in_d_bits_source"]]

editPin -fixOverlap 1 \
 -fixedPin 1 \
 -snap TRACK \
 -spreadDirection clockwise \
 -side Bottom \
 -layer 6 \
 -spreadType side \
 -pin [get_object_name [get_ports "auto_tl_master_xing_out_a_bits_size
                                   auto_tl_master_xing_out_d_bits_data
                                   auto_tl_master_xing_out_d_bits_opcode
                                   auto_tl_master_xing_out_d_bits_param
                                   auto_tl_master_xing_out_d_bits_size
                                   auto_tl_slave_xing_in_a_bits_address"]]

editPin -fixOverlap 1 \
 -fixedPin 1 \
 -snap TRACK \
 -spreadDirection clockwise \
 -side Left \
 -layer 5 \
 -spreadType side \
 -pin [get_object_name [get_ports "auto_tl_slave_xing_in_a_bits_data
                       auto_tl_slave_xing_in_a_bits_mask
                       auto_tl_slave_xing_in_a_bits_opcode
                       auto_tl_slave_xing_in_a_bits_param
                       auto_tl_slave_xing_in_a_bits_size
                       auto_tl_slave_xing_in_a_bits_source
                       auto_tl_slave_xing_in_d_bits_data"]]



