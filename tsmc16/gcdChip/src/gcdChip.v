module gcdChip (
  inout wire [15:0] PAD_N,
  inout wire [15:0] PAD_E,
  inout wire [15:0] PAD_W,
  inout wire [15:0] PAD_S
);

  wire [15:0] PBIDIRN_N_A ;
  wire [15:0] PBIDIRN_N_OE;
  wire [15:0] PBIDIRN_N_IE;
  wire [15:0] PBIDIRN_N_Y ;

  wire [15:0] PBIDIRN_E_A ;
  wire [15:0] PBIDIRN_E_OE;
  wire [15:0] PBIDIRN_E_IE;
  wire [15:0] PBIDIRN_E_Y ;

  wire [15:0] PBIDIRN_S_A ;
  wire [15:0] PBIDIRN_S_OE;
  wire [15:0] PBIDIRN_S_IE;
  wire [15:0] PBIDIRN_S_Y ;

  wire [15:0] PBIDIRN_W_A ;
  wire [15:0] PBIDIRN_W_OE;
  wire [15:0] PBIDIRN_W_IE;
  wire [15:0] PBIDIRN_W_Y ;


  PBIDIRN_18_18_FS_DR_V PBIDIRN_N[15:0] ( .A(PBIDIRN_N_A), .DS0(1'b0), .DS1(1'b0), .IE(PBIDIRN_N_IE),
    .IS(1'b0), .OE(PBIDIRN_N_OE), .PE(1'b0), .POE(1'b0), .PS(1'b0), .RTO(1'b1),
    .SNS(1'b1), .SR(1'b0), .PAD(PAD_N), .Y(PBIDIRN_N_Y) );

  PBIDIRN_18_18_FS_DR_H PBIDIRN_E[15:0] ( .A(PBIDIRN_E_A), .DS0(1'b0), .DS1(1'b0), .IE(PBIDIRN_E_IE),
    .IS(1'b0), .OE(PBIDIRN_E_OE), .PE(1'b0), .POE(1'b0), .PS(1'b0), .RTO(1'b1),
    .SNS(1'b1), .SR(1'b0), .PAD(PAD_E), .Y(PBIDIRN_E_Y) );

  PBIDIRN_18_18_FS_DR_H PBIDIRN_W[15:0] ( .A(PBIDIRN_W_A), .DS0(1'b0), .DS1(1'b0), .IE(PBIDIRN_W_IE),
    .IS(1'b0), .OE(PBIDIRN_W_OE), .PE(1'b0), .POE(1'b0), .PS(1'b0), .RTO(1'b1),
    .SNS(1'b1), .SR(1'b0), .PAD(PAD_W), .Y(PBIDIRN_W_Y) );

  PBIDIRN_18_18_FS_DR_V PBIDIRN_S[15:0] ( .A(PBIDIRN_S_A), .DS0(1'b0), .DS1(1'b0), .IE(PBIDIRN_S_IE),
    .IS(1'b0), .OE(PBIDIRN_S_OE), .PE(1'b0), .POE(1'b0), .PS(1'b0), .RTO(1'b1),
    .SNS(1'b1), .SR(1'b0), .PAD(PAD_S), .Y(PBIDIRN_S_Y) );

  assign PBIDIRN_N_OE[0] = 1'b0;
  assign PBIDIRN_N_OE[1] = 1'b1;
  assign PBIDIRN_N_OE[2] = 1'b0;
  assign PBIDIRN_N_OE[3] = 1'b0;
  assign PBIDIRN_N_OE[4] = 1'b0;
  assign PBIDIRN_N_OE[5] = 1'b1;
  assign PBIDIRN_N_OE[15:6] = {9{1'b0}};

  assign PBIDIRN_N_IE[0] = 1'b1;
  assign PBIDIRN_N_IE[1] = 1'b0;
  assign PBIDIRN_N_IE[2] = 1'b1;
  assign PBIDIRN_N_IE[3] = 1'b1;
  assign PBIDIRN_N_IE[4] = 1'b1;
  assign PBIDIRN_N_IE[5] = 1'b0;
  assign PBIDIRN_N_IE[15:6] = {9{1'b0}};

  assign PBIDIRN_E_OE = {15{1'b0}};
  assign PBIDIRN_E_IE = {15{1'b1}};

  assign PBIDIRN_W_OE = {15{1'b0}};
  assign PBIDIRN_W_IE = {15{1'b1}};

  assign PBIDIRN_S_OE = {15{1'b1}};
  assign PBIDIRN_S_IE = {15{1'b0}};

  gcd i_gcd (
    .clk     (PBIDIRN_N_Y[0]            ), // input  wire [   0:0]
    .req_msg ({PBIDIRN_E_Y, PBIDIRN_W_Y}), // input  wire [  31:0]
    .req_rdy (PBIDIRN_N_A[1]            ), // output wire [   0:0]
    .req_val (PBIDIRN_N_Y[2]            ), // input  wire [   0:0]
    .reset   (PBIDIRN_N_Y[3]            ), // input  wire [   0:0]
    .resp_msg(PBIDIRN_S_A               ), // output wire [  15:0]
    .resp_rdy(PBIDIRN_N_Y[4]            ), // input  wire [   0:0]
    .resp_val(PBIDIRN_N_A[5]            )  // output wire [   0:0]
  );

  endmodule
