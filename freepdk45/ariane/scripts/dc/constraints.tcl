puts "Info: Running script [info script]\n"

create_clock -name core_clock -period 5.6 [get_ports clk_i]

set_input_delay  -clock core_clock -max 0 [all_inputs]
set_input_delay  -clock core_clock -min 0   [all_inputs]

set_output_delay -clock core_clock -max 0 [all_outputs]
set_output_delay -clock core_clock -min 0   [all_outputs]

puts "Info: Completed script [info script]\n"
