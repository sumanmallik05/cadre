# #####
# #allow hold buffers to temporarily overlap cells, will be fixed by refine placement later
# #####
# setOptMode -fixHoldAllowOverlap TRUE

# #####
# #Borrow from the clock skew to help with hold/setup paths
# #####
# setOptMode -usefulSkew false -usefulSkewPostCTS false

# ######
# #In case we want to overprovision slack (positive number) or
# # force it to focus on ones that well exceed the negative number
# ######
# setOptMode -holdTargetSlack 0.020
# setOptMode -setupTargetSlack 0.010

