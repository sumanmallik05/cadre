####################################################################################
#                              ALWAYS_SOURCE PLUG-IN
#####################################################################################
#
# This plug-in script is called from all flow scripts after loading the setup.tcl
# but after to loading the design data.  It can be used to set variables that affect
# non-persistent information
#
#####################################################################################
set t_pitch [dbGet top.fPlan.coreSite.size_y]     ;# Pitch between power rails (standard cell height)
set f_pitch [dbGet head.finGridPitch]             ;# Pitch between fins

# Variables used by Power Planning scripts
set pwr_net_list {VDD VSS}              ;# List of Power nets

set p_rng_w      3.840                  ;# Power ring metal width
set p_rng_s      3.840                  ;# Power ring metal space

set p_str_w      0.720                  ;# Power stripe metal width
set p_str_s      0.360                  ;# Power stripe metal space
set p_str_p      8.000                  ;# Power stripe metal space


# Floorplan Variables
set bondpad_width 36.800
set bondpad_height 71.800
set bondpad_spacing 6
set bondpad_margin 30

