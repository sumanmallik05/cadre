####################################################################################
#                             POST-INIT PLUG-IN
####################################################################################
#
# This plug-in script is called after design import from the run_init.tcl script.
#
# --------------------------------------------------------------------------------
# Can be used for various floorplan related tasks, like:
#              - Die/core boundary
#              - placement of hard macros/blocks
#              - power domain size and clearence surrounding to it
#              - Placement and routing blockages in the floorplan
#              - IO ring creation
#              - PSO planning
# --------------------------------------------------------------------------------
# Specifically, this example includes tasks related to the LP/CPF foundation flow
# including power domain modification and power shut-off planning. The examples
# included here operate based on variables (vars array) defined in the
# OVERLAY/lp_config.tcl file
#
if {[info exists vars(cpf_file)]} {

# --------------------------------------------------------------------------------
# Modify power domains
# --------------------------------------------------------------------------------
# The 'modify_power_domains' procedure is included with the foundation flows
# to help automate power domain modification.  To enable this, please set the
# appropriate variables in the lp_config.tcl and uncomment the following command
#

#	ff_modify_power_domains

# --------------------------------------------------------------------------------
# Power switch insertion
# --------------------------------------------------------------------------------
# The 'add_power_switches' procedure is included with the foundation flows
# to cover common power switch insertion scenarios.  To use, set the appropriate
# variables in the lp_config.tcl file and uncomment the following command.
# --------------------------------------------------------------------------------
# NOTE: This procedure  will NOT COVER THE ALL THE OPTIONS in addPowerSwitch
# For more complicated scenarios, please manually add the addPowerSwitch command
# here with the necessary options.
# --------------------------------------------------------------------------------

#	ff_add_power_switches

}

if {([string tolower $vars(use_pga)] == "true")} {
  # Run PGA to insert endcaps and power grid up to M7
  source $::env(PGA_TECH_FILE)
  source $::env(PGA_DIR)/create_power_grid
  ARM_create_power_grid -mode grid -option_file $::env(PGA_SCRIPTS_DIR)/pga.config
} elseif {$vars(process) < 20} {
  source $::env(INNOVUS_FLOW_DIR)/common/tcl/addEndCap.tcl

  # Manually insert welltaps because they have to be placed after PGA, because PGA inserts endcaps
  # the Foundation Flow by default runs this step before post-init, so we can't use FF
  # We also can't move PGA earlier because it has to be after read_power_intent
  addWellTap -cell [list FILLTIE15_A7P5PP96PTS_C16] \
     -prefix WELLTAP \
     -cellInterval $vars(welltaps,cell_interval) \
     -checkerboard
  verifyWellTap -cells [list FILLTIE15_A7P5PP96PTS_C16] \
     -report $::env(INNOVUS_REPORTS_DIR)/welltap.rpt \
     -rule $vars(welltaps,verify_rule)
}
