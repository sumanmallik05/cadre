####################################################################################
#                             FLOORPLAN SCRIPT
####################################################################################

floorPlan -su $core_aspect_ratio \
             $core_density \
             $core_margin_l \
             $core_margin_b \
             $core_margin_r \
             $core_margin_t

setFlipping s

################################################################################
# MACRO PLACEMENT
################################################################################

# source $::env(INNOVUS_FLOW_DIR)/common/tcl/planDesign.tcl

################################################################################
# I/O PIN PLACEMENT
################################################################################

editPin -fixedPin 1 -fixOverlap 1 -spreadDirection clockwise -side Right -layer 5 -spreadType side -pin {SO {qnt_cnt[0]} {qnt_cnt[1]} {qnt_cnt[2]} {qnt_cnt[3]} {qnt_cnt[4]} {qnt_cnt[5]} {qnt_val[0]} {qnt_val[1]} {qnt_val[2]} {qnt_val[3]} {qnt_val[4]} {qnt_val[5]} {qnt_val[6]} {qnt_val[7]} {rlen[0]} {rlen[1]} {rlen[2]} {rlen[3]} SE SI {size[0]} {size[1]} {size[2]} {size[3]}}

editPin -fixedPin 1 -fixOverlap 1 -spreadDirection clockwise -side Left -layer 5 -spreadType side -pin {ena {amp[0]} {amp[1]} {amp[2]} {amp[3]} {amp[4]} {amp[5]} {amp[6]} {amp[7]} {amp[8]} {amp[9]} {amp[10]} {amp[11]} clk {din[0]} {din[1]} {din[2]} {din[3]} {din[4]} {din[5]} {din[6]} {din[7]} douten dstrb}

