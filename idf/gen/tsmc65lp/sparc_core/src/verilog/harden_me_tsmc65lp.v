// Only expected references are
//  - bram_1rw_wrapper_NAME_DEPTH128_ADDR_WIDTH7_BITMASK_WIDTH78_DATA_WIDTH78
//  - bram_1rw_wrapper_NAME_DEPTH128_ADDR_WIDTH7_BITMASK_WIDTH132_DATA_WIDTH132
//  - bram_1rw_wrapper_NAME_DEPTH256_ADDR_WIDTH8_BITMASK_WIDTH136_DATA_WIDTH136

module bram_1rw_wrapper #(parameter NAME="", DEPTH=1, ADDR_WIDTH=1, BITMASK_WIDTH=1, DATA_WIDTH=1) (
  input                      MEMCLK ,
  input                      RESET_N,
  input                      CE     ,
  input  [   ADDR_WIDTH-1:0] A      ,
  input                      RDWEN  ,
  input  [BITMASK_WIDTH-1:0] BW     ,
  input  [   DATA_WIDTH-1:0] DIN    ,
  output [   DATA_WIDTH-1:0] DOUT
);


  generate
    if (DEPTH==128 && ADDR_WIDTH==7 && BITMASK_WIDTH==78 && DATA_WIDTH==78) begin
      tsmc65lp_1rf_lg7_w78_bit mem (
        .CLK  (MEMCLK  ),
        .Q    (DOUT    ), // out
        .CEN  (~CE      ), // lo true
        .WEN  (BW      ),
        .GWEN (RDWEN), // lo true
        .A    (A       ), // in
        .D    (DIN     ), // in
        .EMA  (3'd3    ), // Extra Margin Adjustment - default value
        .EMAW (2'd1    ), // Extra Margin Adjustment Write - default value
        .RET1N(1'b1    )  // Retention Mode (active low) - disabled
      );
    end
    else if(DEPTH==128 && ADDR_WIDTH==7 && BITMASK_WIDTH==132 && DATA_WIDTH==132) begin
      tsmc65lp_1rf_lg7_w132_bit mem (
        .CLK  (MEMCLK  ),
        .Q    (DOUT    ), // out
        .CEN  (~CE      ), // lo true
        .WEN  (BW      ),
        .GWEN (RDWEN), // lo true
        .A    (A       ), // in
        .D    (DIN     ), // in
        .EMA  (3'd3    ), // Extra Margin Adjustment - default value
        .EMAW (2'd1    ), // Extra Margin Adjustment Write - default value
        .RET1N(1'b1    )  // Retention Mode (active low) - disabled
      );
    end
    else if(DEPTH==256 && ADDR_WIDTH==8 && BITMASK_WIDTH==136 && DATA_WIDTH==136) begin
      tsmc65lp_1rf_lg8_w136_bit mem (
        .CLK  (MEMCLK  ),
        .Q    (DOUT    ), // out
        .CEN  (~CE      ), // lo true
        .WEN  (BW      ),
        .GWEN (RDWEN), // lo true
        .A    (A       ), // in
        .D    (DIN     ), // in
        .EMA  (3'd3    ), // Extra Margin Adjustment - default value
        .EMAW (2'd1    ), // Extra Margin Adjustment Write - default value
        .RET1N(1'b1    )  // Retention Mode (active low) - disabled
      );
    end
    else begin
      unsupported_sram_config mem ( );
    end

  endgenerate
endmodule // bram_1rw_wrapper


module sram_l1d_data_piton (
  input  wire         MEMCLK      ,
  input  wire         RESET_N     ,
  input  wire         CE          ,
  input  wire [  6:0] A           ,
  input  wire         RDWEN       ,
  input  wire [143:0] BW          ,
  input  wire [143:0] DIN         ,
  output wire [143:0] DOUT        ,
  input  wire [4-1:0] BIST_COMMAND,
  input  wire [4-1:0] BIST_DIN    ,
  output reg  [4-1:0] BIST_DOUT   ,
  input  wire [8-1:0] SRAMID
);


  tsmc65lp_1rf_lg7_w144_bit mem (
    .CLK  (MEMCLK  ),
    .Q    (DOUT    ), // out
    .CEN  (~CE      ), // lo true
    .WEN  (BW      ),
    .GWEN (RDWEN), // lo true
    .A    (A       ), // in
    .D    (DIN     ), // in
    .EMA  (3'd3    ), // Extra Margin Adjustment - default value
    .EMAW (2'd1    ), // Extra Margin Adjustment Write - default value
    .RET1N(1'b1    )  // Retention Mode (active low) - disabled
  );

endmodule // bram_1rw_wrapper
