# Parasitivc values from slow_vdd1v2_basicCells.lib
addDeCapCellCandidates DECAP2 0.000640773
addDeCapCellCandidates DECAP3 0.00307041
addDeCapCellCandidates DECAP4 0.00556819
addDeCapCellCandidates DECAP5 0.00795031
addDeCapCellCandidates DECAP6 0.010334
addDeCapCellCandidates DECAP7 0.0127186
addDeCapCellCandidates DECAP8 0.0151039
addDeCapCellCandidates DECAP9 0.0174893
addDeCapCellCandidates DECAP10 0.0198746

#addDeCap -cells {DECAP2 DECAP3 DECAP4 DECAP5 DECAP6 DECAP7 DECAP8 DECAP9 DECAP10} -totCap 500 -effort high