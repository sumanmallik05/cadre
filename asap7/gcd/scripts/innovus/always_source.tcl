####################################################################################
#                              ALWAYS_SOURCE PLUG-IN
#####################################################################################
#
# This plug-in script is called from all flow scripts after loading the setup.tcl
# but after to loading the design data.  It can be used to set variables that affect
# non-persistent information
#
#####################################################################################

set innovusVer [scan [getVersion] %f]
if {$innovusVer <= 15.21} {
  puts "ERROR: THIS DESIGN, SPECIFICALLY THE ASAP7 PDK, REQUIRES A NEWER VERSION OF INNOVUS (GREATER THAN 15.10)"
  exit
}